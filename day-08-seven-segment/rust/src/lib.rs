use itertools::Itertools;
use std::collections::{HashMap, HashSet};
use std::borrow::Borrow;
use std::hash::{Hash, Hasher};
use std::fmt::{self, Debug, Formatter};

#[derive(PartialEq, Eq)]
struct SegmentDisplay {
    segments: HashSet<char>,
    sorted_segments: Vec<char>,
}

impl SegmentDisplay {
    fn new(segments: HashSet<char>) -> Self {
        let sorted_segments = segments.iter().cloned().sorted().collect();
        Self { segments, sorted_segments }
    }

    fn is_superset(&self, other: &Self) -> bool {
        self.segments.is_superset(&other.segments)
    }

    fn len(&self) -> usize {
        self.segments.len()
    }
}

impl Debug for SegmentDisplay {
    fn fmt(&self, f: &mut Formatter<'_>) -> Result<(), fmt::Error> {
        f.debug_tuple("SegmentDisplay")
            .field(&self.sorted_segments)
            .finish()
    }
}

impl Hash for SegmentDisplay {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.sorted_segments.hash(state)
    }
}

impl FromIterator<char> for SegmentDisplay {
    #[inline]
    fn from_iter<T>(iter: T) -> Self
    where
        T: IntoIterator<Item = char>
    {
        Self::new(iter.into_iter().collect())
    }
}

impl From<&str> for SegmentDisplay {
    #[inline]
    fn from(s: &str) -> Self {
        Self::new(s.chars().collect())
    }
}

pub struct SegmentDecoder {
    patterns: Vec<SegmentDisplay>,
    output: Vec<SegmentDisplay>,
}

impl SegmentDecoder {
    pub fn new<I, S>(patterns: I, output: I) -> Self
    where I: IntoIterator<Item=S>, S: Borrow<str>
    {
        let patterns = patterns.into_iter().map(|pattern| pattern.borrow().into()).collect();
        let output = output.into_iter().map(|pattern| pattern.borrow().into()).collect();
        Self { patterns, output }
    }

    pub fn value(&self) -> u32 {
        let one = self.pattern_of_length(2).unwrap();
        let four = self.pattern_of_length(4).unwrap();
        let seven = self.pattern_of_length(3).unwrap();
        let eight = self.pattern_of_length(7).unwrap();

        let three = self.patterns.iter().find(|pattern| pattern.len() == 5 && pattern.is_superset(one)).unwrap();
        let nine = self.patterns.iter().find(|pattern| pattern.len() == 6 && pattern.is_superset(four)).unwrap();
        let six = self.patterns.iter().find(|pattern| pattern.len() == 6 && !pattern.is_superset(one)).unwrap();
        let zero = self.patterns.iter().find(|pattern| pattern.len() == 6 && pattern.is_superset(one) && !pattern.is_superset(four)).unwrap();
        let two = self.patterns.iter().find(|pattern| pattern.len() == 5 && !nine.is_superset(pattern)).unwrap();
        let five = self.patterns.iter().find(|pattern| pattern.len() == 5 && nine.is_superset(pattern) && !pattern.is_superset(one)).unwrap();

        let translate: HashMap<&SegmentDisplay, u32> = vec![
            (zero, 0), (one, 1), (two, 2), (three, 3), (four, 4),
            (five, 5), (six, 6), (seven, 7), (eight, 8), (nine, 9)
        ].into_iter().collect();

        self.output.iter().fold(0, |s, digit| 10*s + translate[&digit])
    }

    fn pattern_of_length(&self, n: usize) -> Option<&SegmentDisplay> {
        self.patterns.iter().find(|pattern| pattern.len() == n)
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    #[rstest]
    #[case(vec!["be", "cfbegad", "cbdgef", "fgaecd", "cgeb", "fdcge", "agebfd", "fecdb", "fabcd", "edb",], vec!["fdgacbe", "cefdb", "cefbgd", "gcbe",], 8394)]
    #[case(vec!["edbfga", "begcd", "cbg", "gc", "gcadebf", "fbgde", "acbgfd", "abcde", "gfcbed", "gfec",], vec!["fcgedb", "cgb", "dgebacf", "gc",], 9781)]
    #[case(vec!["fgaebd", "cg", "bdaec", "gdafb", "agbcfd", "gdcbef", "bgcad", "gfac", "gcb", "cdgabef",], vec!["cg", "cg", "fdcagb", "cbg",], 1197)]
    #[case(vec!["fbegcd", "cbd", "adcefb", "dageb", "afcb", "bc", "aefdc", "ecdab", "fgdeca", "fcdbega",], vec!["efabcd", "cedba", "gadfec", "cb",], 9361)]
    #[case(vec!["aecbfdg", "fbg", "gf", "bafeg", "dbefa", "fcge", "gcbea", "fcaegb", "dgceab", "fcbdga",], vec!["gecf", "egdcabf", "bgf", "bfgea",], 4873)]
    #[case(vec!["fgeab", "ca", "afcebg", "bdacfeg", "cfaedg", "gcfdb", "baec", "bfadeg", "bafgc", "acf",], vec!["gebdcfa", "ecba", "ca", "fadegcb",], 8418)]
    #[case(vec!["dbcfg", "fgd", "bdegcaf", "fgec", "aegbdf", "ecdfab", "fbedc", "dacgb", "gdcebf", "gf",], vec!["cefg", "dcbef", "fcge", "gbcadfe",], 4548)]
    #[case(vec!["bdfegc", "cbegaf", "gecbf", "dfcage", "bdacg", "ed", "bedf", "ced", "adcbefg", "gebcd",], vec!["ed", "bcgafe", "cdgba", "cbgef",], 1625)]
    #[case(vec!["egadfb", "cdbfeg", "cegd", "fecab", "cgb", "gbdefca", "cg", "fgcdab", "egfdb", "bfceg",], vec!["gbdfcae", "bgc", "cg", "cgb",], 8717)]
    #[case(vec!["gcafb", "gcf", "dcaebfg", "ecagb", "gf", "abcdeg", "gaef", "cafbge", "fdbac", "fegbdc",], vec!["fgae", "cfgab", "fg", "bagce",], 4315)]
    fn test_example(#[case] patterns: Vec<&str>, #[case] output: Vec<&str>, #[case] expected: u32) {
        assert_eq!(SegmentDecoder::new(patterns, output).value(), expected);
    }
}
