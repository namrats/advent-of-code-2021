use clap::{App, Arg};
use seven_segment::SegmentDecoder;
use itertools::Itertools;
use std::fs::read_to_string;

fn main() {
    let matches = App::new("segment-decoder")
        .arg(Arg::with_name("input").required(true))
        .get_matches();
    let result: u32 = read_to_string(matches.value_of("input").unwrap())
        .unwrap()
        .lines()
        .map(|line| {
            let (patterns, output) = line.split_once(" | ").unwrap();
            let patterns = patterns.split_whitespace().collect_vec();
            let output = output.split_whitespace().collect_vec();
            SegmentDecoder::new(patterns, output).value()
        }).sum();

    println!("{}", result);
}
