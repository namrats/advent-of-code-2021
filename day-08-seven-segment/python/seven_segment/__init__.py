from collections import Counter


segments_to_values = {
    "abcefg": 0,
    "cf": 1,
    "acdeg": 2,
    "acdfg": 3,
    "bcdf": 4,
    "abdfg": 5,
    "abdefg": 6,
    "acf": 7,
    "abcdefg": 8,
    "abcdfg": 9,
}


def decode_segments(patterns: list[str], output: list[str]) -> int:
    segment_counts = Counter(segment for pattern in patterns for segment in pattern)

    def pattern_of_length(length: int) -> set[str]:
        return next(set(pattern) for pattern in patterns if len(pattern) == length)

    def segments_occuring(times: int) -> set[str]:
        return {
            segment
            for segment, count in segment_counts.items()
            if count == times
        }

    one = pattern_of_length(2)
    four = pattern_of_length(4)
    seven = pattern_of_length(3)

    a = next(iter(seven - one))
    e = next(iter(segments_occuring(4)))
    b = next(iter(segments_occuring(6)))
    f = next(iter(segments_occuring(9)))
    c = next(iter(segments_occuring(8) - set(a)))
    d_g = segments_occuring(7)
    d = next(segment for segment in d_g if segment in four)
    g = next(segment for segment in d_g if segment not in four)

    translate = {a: "a", b: "b", c: "c", d: "d", e: "e", f: "f", g: "g"}
    return sum(
        segments_to_values["".join(sorted(translate[segment] for segment in digit))] * (10 ** i)
        for i, digit in enumerate(output[::-1])
    )
