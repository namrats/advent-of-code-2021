import click
from . import decode_segments


@click.command()
@click.argument("file", type=click.File("r"))
def main(file):
    entries = [
        tuple(list(digits.split()) for digits in line.split("|"))
        for line in file.read().splitlines()
    ]
    print(sum(decode_segments(patterns, output) for patterns, output in entries))
