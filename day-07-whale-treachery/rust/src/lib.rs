pub fn min_alignment_fuel(crab_positions: &[i32]) -> i32 {
    let min_pos = crab_positions.iter().cloned().min().unwrap();
    let max_pos = crab_positions.iter().cloned().max().unwrap();

    (min_pos..=max_pos)
        .map(|pos| {
            crab_positions
                .iter()
                .map(|crab_position| fuel_cost(*crab_position, pos))
                .sum()
        })
        .min()
        .unwrap()
}

fn fuel_cost(start_pos: i32, finish_pos: i32) -> i32 {
    let distance = (start_pos - finish_pos).abs();
    distance * (distance + 1) / 2
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_example() {
        let crab_positions = vec![16, 1, 2, 0, 4, 2, 7, 1, 2, 14];

        assert_eq!(min_alignment_fuel(&crab_positions), 168);
    }
}
