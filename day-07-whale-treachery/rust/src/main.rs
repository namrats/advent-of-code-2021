use clap::{App, Arg};
use std::fs::File;
use std::io::{BufRead, BufReader};
use whale_treachery::min_alignment_fuel;

fn main() {
    let matches = App::new("whale-treachery")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let crab_positions: Vec<i32> =
        BufReader::new(File::open(matches.value_of("input").unwrap()).unwrap())
            .lines()
            .next()
            .unwrap()
            .unwrap()
            .split(",")
            .map(|n| n.parse::<i32>().unwrap())
            .collect();
    println!("{}", min_alignment_fuel(&crab_positions));
}
