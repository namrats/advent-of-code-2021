def min_alignment_fuel(crab_positions: list[int]) -> int:
    min_pos = min(crab_positions)
    max_pos = max(crab_positions)

    return min(
        sum(fuel_cost(crab_position, pos) for crab_position in crab_positions)
        for pos in range(min_pos, max_pos+1)
    )


def fuel_cost(start_pos: int, finish_pos: int) -> int:
    distance = abs(start_pos - finish_pos)
    return distance * (distance + 1) // 2
