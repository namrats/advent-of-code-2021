import click
from . import min_alignment_fuel


@click.command()
@click.argument("file", type=click.File("r"))
def main(file):
    crab_positions = list(map(int, file.readline().split(",")))
    print(min_alignment_fuel(crab_positions))
