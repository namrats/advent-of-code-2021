from whale_treachery import min_alignment_fuel


def test_example():
    crab_positions = [16, 1, 2, 0, 4, 2, 7, 1, 2, 14]
    assert min_alignment_fuel(crab_positions) == 168
