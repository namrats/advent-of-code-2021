use std::{
    hash::Hash,
    iter::{from_fn, FromIterator},
    fmt::Debug,
};

use petgraph::{
    visit::{IntoNeighborsDirected, NodeCount},
    Direction::Outgoing,
};

pub enum VisitationRule {
    Big, Small, StartFinish,
}

pub fn all_paths<TargetColl, F, G>(
    graph: G,
    from: G::NodeId,
    to: G::NodeId,
    visitation_rule: F,
) -> impl Iterator<Item = TargetColl>
where
    G: NodeCount,
    G: IntoNeighborsDirected,
    G::NodeId: Eq + Hash + Debug,
    TargetColl: FromIterator<G::NodeId> + Debug,
    F: Fn(&G::NodeId) -> VisitationRule,
{
    let mut visited: Vec<G::NodeId> = vec![from];
    let mut visited_small_twice: Option<G::NodeId> = None;
    let mut stack = vec![graph.neighbors_directed(from, Outgoing)];

    from_fn(move || {
        while let Some(children) = stack.last_mut() {
            if let Some(child) = children.next() {
                if child == to {
                    let path = visited
                        .iter()
                        .cloned()
                        .chain(Some(to))
                        .collect::<TargetColl>();
                    return Some(path);
                } else {
                    match (visitation_rule(&child), visited_small_twice, visited.contains(&child)) {
                        (VisitationRule::Big, _, _) | (VisitationRule::Small, _, false) => {
                            visited.push(child);
                            stack.push(graph.neighbors_directed(child, Outgoing));
                        }
                        (VisitationRule::Small, None, true) => {
                            visited_small_twice = Some(child);
                            visited.push(child);
                            stack.push(graph.neighbors_directed(child, Outgoing));
                        }
                        _ => ()
                    }
                }
            } else {
                stack.pop();
                let popped = visited.pop();
                if popped == visited_small_twice {
                    visited_small_twice.take();
                }
            }
        }
        None
    })
}
