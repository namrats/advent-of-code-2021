mod algo;

use std::collections::HashMap;
use petgraph::graph::{Graph, UnGraph, NodeIndex};
use self::algo::{VisitationRule, all_paths};

#[derive(PartialEq, Eq, Hash, Debug)]
struct Cave {
    name: String,
}

impl Cave {
    fn new(name: &str) -> Self {
        Self {
            name: name.to_string(),
        }
    }

    fn visitation_rule(&self) -> VisitationRule {
        if ["start", "finish"].contains(&self.name.as_str()) {
            VisitationRule::StartFinish
        } else if self.name.chars().next().unwrap().is_uppercase() {
            VisitationRule::Big
        } else {
            VisitationRule::Small
        }
    }
}

pub struct CaveMap {
    graph: UnGraph<Cave, ()>,
    node_names: HashMap<String, NodeIndex>
}

impl CaveMap {
    pub fn new(edges: Vec<(&str, &str)>) -> Self {
        let mut node_names = HashMap::new();
        let mut graph = Graph::new_undirected();
        for (a, b) in edges {
            let a_node = *node_names.entry(a.to_string()).or_insert_with(|| { graph.add_node(Cave::new(a)) });
            let b_node = *node_names.entry(b.to_string()).or_insert_with(|| { graph.add_node(Cave::new(b)) });
            graph.add_edge(a_node, b_node, ());
        }
        Self { graph, node_names }
    }

    pub fn path_count(&self) -> usize {
        let start = self.node_names["start"];
        let end = self.node_names["end"];

        all_paths::<Vec<_>, _, _>(&self.graph, start, end, |n| self.graph[*n].visitation_rule()).count()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;

    #[rstest]
    #[case(36, vec![
        ("start", "A"),
        ("start", "b"),
        ("A", "c"),
        ("A", "b"),
        ("b", "d"),
        ("A", "end"),
        ("b", "end"),
    ])]
    #[case(103, vec![
        ("dc", "end"),
        ("HN", "start"),
        ("start", "kj"),
        ("dc", "start"),
        ("dc", "HN"),
        ("LN", "dc"),
        ("HN", "end"),
        ("kj", "sa"),
        ("kj", "HN"),
        ("kj", "dc"),
    ])]
    #[case(3509, vec![
        ("fs", "end"),
        ("he", "DX"),
        ("fs", "he"),
        ("start", "DX"),
        ("pj", "DX"),
        ("end", "zg"),
        ("zg", "sl"),
        ("zg", "pj"),
        ("pj", "he"),
        ("RW", "he"),
        ("fs", "DX"),
        ("pj", "RW"),
        ("zg", "RW"),
        ("start", "pj"),
        ("he", "WI"),
        ("zg", "he"),
        ("pj", "fs"),
        ("start", "RW"),
    ])]
    fn test_example(#[case] expected: usize, #[case] edges: Vec<(&str, &str)>) {
        assert_eq!(CaveMap::new(edges).path_count(), expected);
    }
}
