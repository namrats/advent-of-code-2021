use std::fs::read_to_string;
use clap::{App, Arg};
use itertools::Itertools;
use passage_pathing::CaveMap;

fn main() {
    let matches = App::new("passage-pathing")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let content = read_to_string(matches.value_of("input").unwrap()).unwrap();
    let input = content
        .lines()
        .map(|line| line.split_once("-").unwrap())
        .collect_vec();

    println!("{}", CaveMap::new(input).path_count());
}
