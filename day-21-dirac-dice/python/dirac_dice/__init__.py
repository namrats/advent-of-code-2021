from collections import Counter
from functools import cache


ROLL_COUNTS = Counter({6: 7, 5: 6, 7: 6, 4: 3, 8: 3, 3: 1, 9: 1})


def max_winner(p0, p1):
    return max(winners(p0, p1, 0, 0))


@cache
def winners(p0, p1, s0, s1):
    r0 = 0
    r1 = 0

    for roll, count in ROLL_COUNTS.items():
        np0 = (p0 + roll - 1) % 10 + 1
        ns0 = s0 + np0
        if ns0 >= 21:
            r0 += count
        else:
            w1, w0 = winners(p1, np0, s1, ns0)
            r0 += w0 * count
            r1 += w1 * count
    return (r0, r1)
