from dirac_dice import max_winner


def test_example():
    actual = max_winner(4, 8)
    assert actual == 444356092776315
