use clap::{App, Arg, SubCommand};
use smoke_basin::{basin_sizes, low_points};
use std::fs::read_to_string;

fn main() {
    let matches = App::new("smoke-basin")
        .subcommand(SubCommand::with_name("low-points").arg(Arg::with_name("input").required(true)))
        .subcommand(
            SubCommand::with_name("basin-sizes").arg(Arg::with_name("input").required(true)),
        )
        .get_matches();
    match matches.subcommand() {
        ("low-points", Some(matches)) => {
            let input = read_to_string(matches.value_of("input").unwrap())
                .unwrap()
                .lines()
                .map(|line| line.chars().map(|c| c.to_digit(10).unwrap()).collect())
                .collect();
            let result: u32 = low_points(input).into_iter().map(|n| n + 1).sum();
            println!("{}", result);
        }
        ("basin-sizes", Some(matches)) => {
            let input = read_to_string(matches.value_of("input").unwrap())
                .unwrap()
                .lines()
                .map(|line| line.chars().map(|c| c.to_digit(10).unwrap()).collect())
                .collect();
            let result: usize = basin_sizes(input).into_iter().take(3).product();
            println!("{}", result);
        }
        _ => (),
    }
}
