use itertools::Itertools;
use petgraph::{algo::kosaraju_scc, graph::UnGraph};
use std::{cmp::Reverse, iter::from_fn};

fn neighbors(
    map: &Vec<Vec<u32>>,
    i: usize,
    j: usize,
    height: usize,
    width: usize,
) -> Vec<((usize, usize), u32)> {
    let mut result = Vec::new();
    if i > 0 {
        result.push(((i - 1, j), map[j][i - 1]));
    }
    if j > 0 {
        result.push(((i, j - 1), map[j - 1][i]));
    }
    if i + 1 < width {
        result.push(((i + 1, j), map[j][i + 1]));
    }
    if j + 1 < height {
        result.push(((i, j + 1), map[j + 1][i]));
    }
    result
}

fn neighbor_window<'a>(
    map: &'a Vec<Vec<u32>>,
) -> impl 'a + Iterator<Item = ((usize, usize), u32, Vec<((usize, usize), u32)>)> {
    let mut i = 0;
    let mut j = 0;
    let height = map.len();
    let width = map[0].len();

    from_fn(move || {
        if i == width {
            i = 0;
            j += 1;
        }
        if j == height {
            return None;
        }
        let result = ((i, j), map[j][i], neighbors(map, i, j, height, width));
        i += 1;
        return Some(result);
    })
}

pub fn low_points(map: Vec<Vec<u32>>) -> Vec<u32> {
    neighbor_window(&map)
        .filter(|(_, p, neighbors)| neighbors.into_iter().all(|(_, neighbor)| p < neighbor))
        .map(|(_, p, _)| p)
        .collect()
}

pub fn basin_sizes(map: Vec<Vec<u32>>) -> Vec<usize> {
    let width = map[0].len();
    let edges = neighbor_window(&map).flat_map(|((i, j), p, neighbors)| {
        neighbors
            .into_iter()
            .filter(move |(_, neighbor)| neighbor != &9 && &p < neighbor)
            .map(move |((m, n), _)| ((i + width * j) as u32, (m + width * n) as u32))
    });
    let graph = UnGraph::<(), ()>::from_edges(edges);
    kosaraju_scc(&graph)
        .into_iter()
        .map(|component| component.len())
        .sorted_by_key(|n| Reverse(*n))
        .collect()
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use rstest::{fixture, rstest};

    #[fixture]
    fn map() -> Vec<Vec<u32>> {
        let input = indoc! {
        "
            2199943210
            3987894921
            9856789892
            8767896789
            9899965678
            "};
        input
            .lines()
            .map(|row| row.chars().map(|c| c.to_digit(10).unwrap()).collect())
            .collect()
    }

    #[rstest]
    fn test_low_points(map: Vec<Vec<u32>>) {
        assert_eq!(low_points(map), vec![1, 0, 5, 5]);
    }

    #[rstest]
    fn test_basins(map: Vec<Vec<u32>>) {
        let actual = basin_sizes(map);
        assert_eq!(actual[..3], vec![14, 9, 9]);
    }
}
