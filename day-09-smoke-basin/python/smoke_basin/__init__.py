from itertools import chain
from more_itertools import windowed
from igraph import Graph


def neighbor_window(map):
    return (
        ((i, j), w[1][1], [(c, neighbor) for c, neighbor in [((i-1, j), w[1][0]), ((i, j-1), w[0][1]), ((i, j+1), w[2][1]), ((i+1, j), w[1][2])] if neighbor is not None])
        for j, rows in enumerate(windowed(chain([[None]*len(map[0])], map, [[None]*len(map[0])]), 3))
        for i, w in enumerate(zip(*(windowed(chain([None], row, [None]), 3) for row in rows)))
    )


def low_points(map: list[list[int]]) -> list[int]:
    return [
        p
        for _, p, neighbors in neighbor_window(map)
        if all(p < neighbor for _, neighbor in neighbors)
    ]


def risk_level(height: int) -> int:
    return height + 1


def basin_sizes(map: list[list[int]]) -> list[int]:
    g = Graph()
    height = len(map)
    width = len(map[0])
    g.add_vertices(height * width)

    for cp, p, neighbors in neighbor_window(map):
        for cn, neighbor in neighbors:
            if p < neighbor and neighbor != 9:
                g.add_edge(cp[0]+width*cp[1], cn[0]+width*cn[1])

    return list(sorted((len(component) for component in g.components()), reverse=True))
