import click
from functools import reduce
from operator import mul
from . import low_points, risk_level, basin_sizes


@click.group()
def main():
    pass


@main.command("low-points")
@click.argument("file", type=click.File("r"))
def low_points_command(file):
    map = [
        [int(n) for n in row]
        for row in file.read().splitlines()
    ]

    print(sum(risk_level(height) for height in low_points(map)))


@main.command("basins")
@click.argument("file", type=click.File("r"))
def basin_sizes_command(file):
    map = [
        [int(n) for n in row]
        for row in file.read().splitlines()
    ]

    print(reduce(mul, basin_sizes(map)[:3], 1))
