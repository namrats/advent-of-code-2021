import pytest
from inspect import cleandoc
from smoke_basin import low_points, basin_sizes


@pytest.fixture
def map():
    map = cleandoc("""
    2199943210
    3987894921
    9856789892
    8767896789
    9899965678
    """)

    return [
        [int(n) for n in row]
        for row in map.splitlines()
    ]


def test_low_points(map):
    assert low_points(map) == [1, 0, 5, 5]


def test_basins(map):
    actual = basin_sizes(map)
    assert actual[:3] == [
        14, 9, 9
    ]
