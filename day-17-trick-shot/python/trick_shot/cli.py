import click
import re
from trick_shot import calculate_trick_shot, calculate_all_shots


INPUT_RE = r"target area: x=(-?\d+)..(-?\d+), y=(-?\d+)..(-?\d+)"


@click.command()
@click.argument("file", type=click.File("r"))
@click.option("-a", "--all", "all_shots", is_flag=True)
def main(file, all_shots):
    m = re.match(INPUT_RE, file.read())
    target_area = tuple(int(m.group(n)) for n in range(1, 5))
    if all_shots:
        print(len(calculate_all_shots(target_area)))
    else:
        print(calculate_trick_shot(target_area))
