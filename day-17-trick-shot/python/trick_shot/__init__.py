from typing import Tuple


def calculate_trick_shot(target_area: Tuple[int, int, int, int]):
    min_x, max_x, min_y, max_y = target_area
    vx = next(n for n in range(min_x) if min_x <= n * (n + 1) / 2 <= max_x)
    vy = abs(min_y) - 1

    shot = (vx, vy)

    high_point = max(y for _, y in Simulator(target_area).trajectory(shot))
    return (high_point, shot)


def calculate_all_shots(target_area: Tuple[int, int, int, int]):
    min_x, max_x, min_y, max_y = target_area
    simulator = Simulator(target_area)

    return {
        (vx, vy)
        for vx in range(max_x + 1)
        for vy in range(min_y, abs(min_y))
        if simulator.hits_target((vx, vy))
    }


class Simulator:
    def __init__(self, target_area: Tuple[int, int, int, int]):
        self.min_x, self.max_x, self.min_y, self.max_y = target_area

    def trajectory(self, shot: Tuple[int, int]):
        position = (0, 0)
        velocity = shot

        while (
            position[0] <= self.max_x
            and position[1] >= self.min_y
            and (position[0] < self.min_x or position[1] > self.max_y)
        ):
            position = (position[0] + velocity[0], position[1] + velocity[1])
            velocity = (max(0, velocity[0] - 1), velocity[1] - 1)
            yield position

    def hits_target(self, shot: Tuple[int, int]) -> bool:
        *_, position = self.trajectory(shot)
        return (
            self.min_x <= position[0] <= self.max_x
            and self.min_y <= position[1] <= self.max_y
        )
