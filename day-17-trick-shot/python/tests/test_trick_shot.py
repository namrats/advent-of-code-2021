import pytest
from pathlib import Path
from trick_shot import Simulator, calculate_trick_shot, calculate_all_shots


def test_example():
    target_area = (20, 30, -10, -5)
    high_point, shot = calculate_trick_shot(target_area)
    assert high_point == 45
    assert shot == (6, 9)


@pytest.mark.parametrize(
    "shot",
    [
        (7, 2),
        (6, 3),
        (9, 0),
        (6, 9),
    ],
)
def test_simulator_hits(shot):
    print(shot)
    target_area = (20, 30, -10, -5)
    simulator = Simulator(target_area)
    print(list(simulator.trajectory(shot)))
    assert simulator.hits_target(shot)


class TestAllShots:
    @pytest.fixture
    def expected(self):
        with Path(__file__).with_name("all_shots.txt").open() as file:
            return {
                tuple(int(c) for c in line.split(","))
                for line in file.read().splitlines()
            }

    def test_calculate_all_shots(self, expected):
        target_area = (20, 30, -10, -5)
        actual = calculate_all_shots(target_area)

        assert actual == expected


def test_real_input():
    vx = next(n for n in range(277) if 277 <= (n * (n + 1) / 2) <= 318)

    shot = (vx, 91)
    print(shot)
    target_area = (277, 318, -92, -53)
    simulator = Simulator(target_area)
    print(list(simulator.trajectory(shot)))
    assert simulator.hits_target(shot)


@pytest.mark.parametrize(
    "shot",
    [
        (17, -4),
    ],
)
def test_simulator_misses(shot):
    target_area = (20, 30, -10, -5)
    simulator = Simulator(target_area)
    assert not simulator.hits_target(shot)
