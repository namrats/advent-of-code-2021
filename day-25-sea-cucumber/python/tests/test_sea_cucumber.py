import pytest
from inspect import cleandoc
from pathlib import Path
from sea_cucumber import Simulator


class TestSmallExample:
    @pytest.mark.parametrize("input,expected", [
        ("...>>>>>...", "...>>>>.>.."),
        ("...>>>>.>..", "...>>>.>.>."),
        (
            cleandoc("""
            ..........
            .>v....v..
            .......>..
            ..........
            """),
            cleandoc("""
            ..........
            .>........
            ..v....v>.
            ..........
            """)
        ),
    ])
    def test_example(self, input, expected):
        simulator = Simulator(input)
        simulator.run()
        assert simulator.grid() == expected


def parse_section(section: str):
    step_count, grid = section.split("\n", maxsplit=1)
    return (int(step_count), grid)


def big_examples():
    with Path(__file__).with_name("big_example.txt").open() as file:
        return [parse_section(section) for section in file.read().split("\n\n")]


class TestBigExample:
    @pytest.fixture
    def grid(self):
        return cleandoc("""
            v...>>.vv>
            .vv>>.vv..
            >>.>v>...v
            >>v>>.>.v.
            v>v.vv.v..
            >.>>..v...
            .vv..>.>v.
            v.v..>>v.v
            ....v..v.>
        """)

    @pytest.mark.parametrize("step_count, expected", big_examples())
    def test_example(self, grid, step_count, expected):
        simulator = Simulator(grid)
        for _ in range(step_count):
            simulator.run()

        assert simulator.grid() == expected.strip()

    def test_stops(self, grid):
        simulator = Simulator(grid)
        assert simulator.run_until_stop() == 58
