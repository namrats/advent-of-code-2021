from typing import Tuple


class Simulator:
    def __init__(self, grid: str):
        grid = [
            [cell for cell in row]
            for row in grid.splitlines()]

        self.height = len(grid)
        self.width = len(grid[0])
        self.east_herd = {
            (i, j)
            for j, row in enumerate(grid)
            for i, cell in enumerate(row)
            if cell == ">"
        }
        self.south_herd = {
            (i, j)
            for j, row in enumerate(grid)
            for i, cell in enumerate(row)
            if cell == "v"
        }

    def move(self, direction: Tuple[int, int], position: Tuple[int, int]) -> Tuple[int, int]:
        new_position = (
            (position[0] + direction[0] + self.width) % self.width,
            (position[1] + direction[1] + self.height) % self.height,
        )

        if new_position not in self.east_herd and new_position not in self.south_herd:
            return new_position
        else:
            return position

    def run(self) -> bool:
        old_east_herd = self.east_herd
        self.east_herd = {
            self.move((1, 0), position)
            for position in self.east_herd
        }

        old_south_herd = self.south_herd
        self.south_herd = {
            self.move((0, 1), position)
            for position in self.south_herd
        }

        return old_east_herd != self.east_herd or old_south_herd != self.south_herd

    def run_until_stop(self) -> int:
        i = 1
        while self.run():
            i += 1

        return i

    def grid(self) -> str:
        return "\n".join(
            "".join(
                ">" if (i, j) in self.east_herd else "v" if (i, j) in self.south_herd else "."
                for i in range(self.width)
            )
            for j in range(self.height)
        )
