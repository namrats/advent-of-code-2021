import click
from sea_cucumber import Simulator


@click.command()
@click.argument("file", type=click.File("r"))
def main(file):
    simulator = Simulator(file.read())
    print(simulator.run_until_stop())
