use sonar_sweep::sweep;
use clap::{App, Arg};
use std::io::{BufRead, BufReader};
use std::fs::File;

fn main() {
    let matches = App::new("sonar_sweep")
        .arg(Arg::with_name("input").required(true))
        .get_matches();
    let input: Vec<_> = BufReader::new(File::open(matches.value_of("input").unwrap()).unwrap())
        .lines()
        .map(|line| line.unwrap().parse::<i32>().unwrap())
        .collect();
    println!("{}", sweep(&input));
}
