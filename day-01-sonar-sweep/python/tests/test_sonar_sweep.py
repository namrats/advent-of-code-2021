from sonar_sweep import sweep


def test_example():
    measurements = [
        199,
        200,
        208,
        210,
        200,
        207,
        240,
        269,
        260,
        263,
    ]

    assert sweep(measurements) == 5
