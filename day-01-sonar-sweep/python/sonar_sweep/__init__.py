from typing import Iterator
from more_itertools import ilen, windowed


def sweep(measurements: Iterator[int]) -> int:
    return ilen(
        () for m, n in windowed(map(sum, windowed(measurements, 3)), 2) if m < n
    )
