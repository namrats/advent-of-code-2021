import click
from sonar_sweep import sweep


@click.command()
@click.argument('file', type=click.File('r'))
def main(file):
    print(sweep(map(int, file)))
