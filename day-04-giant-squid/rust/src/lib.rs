use std::collections::HashMap;

pub struct BingoBoard {
    numbers: HashMap<u32, (usize, usize)>,
    marked_rows: [u8; 5],
    marked_columns: [u8; 5],
    score: u32,
}

impl BingoBoard {
    pub fn new(numbers: Vec<Vec<u32>>) -> Self {
        Self {
            numbers: numbers.iter().enumerate().flat_map(|(i, row)| row.iter().enumerate().map(move |(j, n)| (*n, (i, j)))).collect(),
            marked_rows: [0; 5],
            marked_columns: [0; 5],
            score: 0,
        }
    }

    pub fn mark_number(&mut self, number: u32) {
        if self.numbers.contains_key(&number) {
            let (i, j) = self.numbers[&number];
            self.marked_rows[i] |= 1<<j;
            self.marked_columns[j] |= 1<<i;
        }

        if self.score == 0 && self.is_winner() {
            let unmarked_numbers = self.numbers.iter().filter(|(_, (i, j))| self.marked_rows[*i] & (1<<j) == 0).map(|(n, _)| n);
            self.score = number * unmarked_numbers.sum::<u32>();
        }
    }

    pub fn is_winner(&self) -> bool {
        self.score > 0
        || self.marked_rows.iter().any(|row| *row == 0b11111u8)
        || self.marked_columns.iter().any(|column| *column == 0b11111u8)
    }

    pub fn score(&self) -> u32 {
        self.score
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::{fixture, rstest};

    #[fixture]
    fn drawn_numbers() -> Vec<u32> {
        vec![7,4,9,5,11,17,23,2,0,14,21,24]
    }

    #[rstest]
    #[case(vec![
        vec![22, 13, 17, 11,  0],
        vec![8,  2, 23,  4, 24],
        vec![21,  9, 14, 16,  7],
        vec![6, 10,  3, 18,  5],
        vec![1, 12, 20, 15, 19],
    ], false, 0)]
    #[case(vec![
        vec![14, 21, 17, 24,  4],
        vec![10, 16, 15,  9, 19],
        vec![18,  8, 23, 26, 20],
        vec![22, 11, 13,  6,  5],
        vec![2,  0, 12,  3,  7],
    ], true, 4512)]
    fn test_example(drawn_numbers: Vec<u32>, #[case] board_numbers: Vec<Vec<u32>>, #[case] winner: bool, #[case] score: u32) {
        let mut board = BingoBoard::new(board_numbers);
        for number in drawn_numbers {
            board.mark_number(number);
        }

        assert_eq!(board.is_winner(), winner);
        assert_eq!(board.score(), score);
    }
}
