use giant_squid::BingoBoard;
use clap::{App, Arg};
use std::fs::File;
use std::io::{BufRead, BufReader};
use itertools::Itertools;

fn main() {
    let matches = App::new("giant-squid")
        .arg(Arg::with_name("lose").short("l").long("lose"))
        .arg(Arg::with_name("input").required(true))
        .get_matches();
    let mut lines = BufReader::new(File::open(matches.value_of("input").unwrap()).unwrap()).lines();
    let numbers: Vec<u32> = lines.next().unwrap().unwrap().split(",").map(|n| n.parse::<u32>().unwrap()).collect();
    let _ = lines.next().unwrap();
    let boards: Vec<BingoBoard> = lines.map(|line| line.unwrap()).filter(|line| !line.is_empty()).chunks(5).into_iter().map(|rows| BingoBoard::new(rows.map(|row| row.split_ascii_whitespace().map(|n| n.parse::<u32>().unwrap()).collect()).collect())).collect();

    if matches.is_present("lose") {
        println!("{}", losing_score(numbers, boards));
    } else {
        println!("{}", winning_score(numbers, boards));
    }
}

fn losing_score(numbers: Vec<u32>, mut boards: Vec<BingoBoard>) -> u32 {
    for number in numbers {
        for ref mut board in &mut boards {
            board.mark_number(number);
        }
        if boards.len() == 1 && boards[0].is_winner() {
            return boards[0].score();
        }
        boards = boards.into_iter().filter(|board| !board.is_winner()).collect();
    }
    0
}

fn winning_score(numbers: Vec<u32>, mut boards: Vec<BingoBoard>) -> u32 {
    for number in numbers {
        for ref mut board in &mut boards {
            board.mark_number(number);
            if board.is_winner() {
                return board.score();
            }
        }
    }
    0
}
