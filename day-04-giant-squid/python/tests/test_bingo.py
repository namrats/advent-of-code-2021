import pytest
from giant_squid.bingo import BingoBoard


@pytest.fixture
def drawn_numbers():
    return [7,4,9,5,11,17,23,2,0,14,21,24]


@pytest.mark.parametrize("board_numbers,winner,score", [
    (
        [
            [22, 13, 17, 11,  0],
            [8,  2, 23,  4, 24],
            [21,  9, 14, 16,  7],
            [6, 10,  3, 18,  5],
            [1, 12, 20, 15, 19],
        ], False, 0
    ),
    (
        [
            [14, 21, 17, 24,  4],
            [10, 16, 15,  9, 19],
            [18,  8, 23, 26, 20],
            [22, 11, 13,  6,  5],
            [2,  0, 12,  3,  7],
        ], True, 4512
    )
])
def test_example(board_numbers, winner, score, drawn_numbers):
    board = BingoBoard(board_numbers)

    for number in drawn_numbers:
        board.mark_number(number)

    assert board.is_winner() == winner
    assert board.score == score
