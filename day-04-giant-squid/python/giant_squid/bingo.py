class BingoBoard:
    def __init__(self, numbers: list[list[int]]):
        self.numbers = {
            number: (i, j)
            for i, row in enumerate(numbers)
            for j, number in enumerate(row)
        }
        self.marked = [[False for _ in row] for row in numbers]
        self.score = 0

    def mark_number(self, number: int):
        if number in self.numbers:
            i, j = self.numbers[number]
            self.marked[i][j] = True

        if not self.score and self.is_winner():
            s = sum(number for number, (i, j) in self.numbers.items() if not self.marked[i][j])
            self.score = s * number

    def is_winner(self):
        return self.score > 0 or (
            any(all(row) for row in self.marked)
            or any(all(column) for column in zip(*self.marked))
        )
