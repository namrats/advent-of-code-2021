import click
from operator import not_
from .bingo import BingoBoard
from more_itertools import split_at


@click.command()
@click.option('-l', '--lose', is_flag=True)
@click.argument('file', type=click.File('r'))
def main(file, lose):
    numbers = [int(number) for number in file.readline().rstrip().split(",")]
    file.readline()

    boards = [
        BingoBoard([
            [int(number) for number in board_numbers.split()]
            for board_numbers in boards
        ])
        for boards in split_at(file.read().splitlines(), not_)
    ]

    if lose:
        print(losing_score(numbers, boards))
    else:
        print(winning_score(numbers, boards))


def losing_score(numbers: list[int], boards: list[BingoBoard]) -> int:
    for number in numbers:
        for board in boards:
            board.mark_number(number)
        if len(boards) == 1 and boards[0].is_winner():
            return boards[0].score
        boards = [board for board in boards if not board.is_winner()]
    return 0


def winning_score(numbers: list[int], boards: list[BingoBoard]) -> int:
    for number in numbers:
        for board in boards:
            board.mark_number(number)
            if board.is_winner():
                return board.score
    return 0
