import pytest
from extended_polymerization import PairInsertionRules, analyze_frequencies


def u(n, start):
    """
    Sanity check for testing the size of a polymer after n insertions
    """
    if n == 0:
        return start
    else:
        return 2 * u(n-1, start) - 1


@pytest.fixture
def rules():
    return PairInsertionRules([
        ("CH", "B"),
        ("HH", "N"),
        ("CB", "H"),
        ("NH", "C"),
        ("HB", "C"),
        ("HC", "B"),
        ("HN", "C"),
        ("NN", "C"),
        ("BH", "H"),
        ("NC", "B"),
        ("NB", "B"),
        ("BN", "B"),
        ("BB", "N"),
        ("BC", "B"),
        ("CC", "N"),
        ("CN", "C"),
    ])


@pytest.mark.parametrize("step_count,expected", [
    (10, 1588),
    (40, 2188189693529),
])
def test_example(rules, step_count, expected):
    polymer_template = "NNCB"

    frequencies = rules.apply(polymer_template, step_count)

    assert analyze_frequencies(frequencies) == expected
    assert sum(frequencies.values()) == u(step_count, len(polymer_template))
