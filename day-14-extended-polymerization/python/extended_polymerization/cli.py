import click
from extended_polymerization import PairInsertionRules, analyze_frequencies


@click.command()
@click.argument("file", type=click.File("r"))
@click.option("-n", "step_count", default=40)
def main(file, step_count):
    (polymer_template, rule_text) = file.read().split("\n\n")
    rules = PairInsertionRules(line.split(" -> ") for line in rule_text.splitlines())

    frequencies = rules.apply(polymer_template, step_count)

    print(analyze_frequencies(frequencies))
