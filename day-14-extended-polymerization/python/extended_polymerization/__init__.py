from collections import Counter
from typing import Tuple, Iterable
from more_itertools import windowed
from functools import cache


class PairInsertionRules:
    def __init__(self, rules: Iterable[Tuple[str, str]]):
        self.rules = {tuple(pair): insertion for pair, insertion in rules}

    def apply(self, polymer_template: str, step_count: int) -> Counter():
        pairs = Counter(windowed(polymer_template, 2))
        return sum(
            (self.apply_pair(pair, step_count) for pair in pairs),
            start=Counter(polymer_template)
        )

    @cache  # remove this and 💥
    def apply_pair(self, pair, step_count):
        if step_count == 0:
            return Counter()
        else:
            insertion = self.rules[pair]
            return (
                self.apply_pair((pair[0], insertion), step_count - 1)
                + self.apply_pair((insertion, pair[1]), step_count - 1)
                + Counter(insertion)
            )


def analyze_frequencies(frequencies: Counter[str]):
    most_common = max(frequencies.values())
    least_common = min(frequencies.values())

    return most_common - least_common
