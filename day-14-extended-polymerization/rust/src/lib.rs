use itertools::Itertools;
use std::collections::HashMap;

pub struct PairInsertionRules {
    rules: HashMap<(char, char), char>,
}

impl PairInsertionRules {
    pub fn new<'a, I>(rules: I) -> Self
    where
        I: IntoIterator<Item = (&'a str, &'a str)>,
    {
        let rules = rules
            .into_iter()
            .map(|(pair, insertion)| {
                (
                    pair.chars().collect_tuple().unwrap(),
                    insertion.chars().next().unwrap(),
                )
            })
            .collect();
        Self { rules }
    }

    pub fn apply(&self, polymer_template: &str, step_count: usize) -> usize {
        let mut counts = polymer_template.chars().counts();
        let mut pairs = polymer_template.chars().tuple_windows::<(_, _)>().counts();
        for _ in 0..step_count {
            let mut new_pairs = HashMap::new();
            for (pair, count) in pairs {
                let insertion = self.rules[&pair];
                *counts.entry(insertion).or_default() += count;
                *new_pairs.entry((pair.0, insertion)).or_default() += count;
                *new_pairs.entry((insertion, pair.1)).or_default() += count;
            }
            pairs = new_pairs;
        }
        counts.values().max().unwrap() - counts.values().min().unwrap()
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::{fixture, rstest};

    #[fixture]
    fn rules() -> PairInsertionRules {
        PairInsertionRules::new(vec![
            ("CH", "B"),
            ("HH", "N"),
            ("CB", "H"),
            ("NH", "C"),
            ("HB", "C"),
            ("HC", "B"),
            ("HN", "C"),
            ("NN", "C"),
            ("BH", "H"),
            ("NC", "B"),
            ("NB", "B"),
            ("BN", "B"),
            ("BB", "N"),
            ("BC", "B"),
            ("CC", "N"),
            ("CN", "C"),
        ])
    }

    #[rstest]
    #[case(10, 1588)]
    #[case(40, 2188189693529)]
    fn test_example(rules: PairInsertionRules, #[case] step_count: usize, #[case] expected: usize) {
        let polymer_template = "NNCB";

        assert_eq!(rules.apply(polymer_template, step_count), expected);
    }
}
