use clap::{App, Arg};
use extended_polymerization::PairInsertionRules;
use std::fs::read_to_string;

fn main() {
    let matches = App::new("extended-polymerization")
        .arg(Arg::with_name("input").required(true))
        .arg(Arg::with_name("step_count").short("n").default_value("40"))
        .get_matches();

    let contents = read_to_string(matches.value_of("input").unwrap()).unwrap();
    let (polymer_template, rule_text) = contents.split_once("\n\n").unwrap();

    let rules = PairInsertionRules::new(
        rule_text
            .lines()
            .map(|line| line.split_once(" -> ").unwrap()),
    );
    let step_count = matches
        .value_of("step_count")
        .unwrap()
        .parse::<usize>()
        .unwrap();
    println!("{}", rules.apply(&polymer_template, step_count));
}
