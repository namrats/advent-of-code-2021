use petgraph::{
    algo::dijkstra,
    graph::{DiGraph, Graph},
};

pub struct Cave {
    g: DiGraph<(), u32>,
    source: u32,
    target: u32,
}

impl Cave {
    fn node_id(width: usize, i: usize, j: usize) -> u32 {
        (i + j * width) as u32
    }

    fn neighbors(width: usize, height: usize, i: usize, j: usize) -> Vec<(usize, usize)> {
        let mut result = Vec::new();
        if i > 0 {
            result.push((i-1, j));
        }
        if i < width - 1 {
            result.push((i+1, j));
        }
        if j > 0 {
            result.push((i, j-1));
        }
        if j < height - 1 {
            result.push((i, j+1));
        }
        result
    }

    pub fn grow(risk_levels: Vec<Vec<u32>>) -> Self {
        let bigger_risk_levels: Vec<Vec<u32>> =
            (0..5).flat_map(|j| {
                risk_levels.clone().into_iter()
                    .map(move |row| {
                        (0..5).flat_map(|i| {
                            row.clone().into_iter().map(move |r| (r - 1 + i + j) % 9 + 1)
                        }).collect()
                    })
            })
            .collect();

        Self::new(bigger_risk_levels)
    }

    pub fn new(risk_levels: Vec<Vec<u32>>) -> Self {
        let height = risk_levels.len();
        let width = risk_levels[0].len();

        let it = risk_levels.into_iter()
            .enumerate()
            .flat_map(|(j, row)| row.into_iter().enumerate().map(move |(i, r)| (i, j, r)))
            .flat_map(|(i, j, r)| {
                Self::neighbors(width, height, i, j)
                .into_iter()
                .map(move |n| (Self::node_id(width, n.0, n.1), Self::node_id(width, i, j), r))
            });
        let g = Graph::<(), u32>::from_edges(it);

        Self {
            g,
            source: 0,
            target: (height * width - 1) as u32,
        }
    }

    pub fn lowest_risk(&self) -> u32 {
        dijkstra(&self.g, self.source.into(), Some(self.target.into()), |e| *e.weight())[&self.target.into()]
    }
}


#[cfg(test)]
mod tests {
    use super::*;
    use rstest::{fixture, rstest};
    use indoc::indoc;

    #[fixture]
    fn risk_levels() -> Vec<Vec<u32>> {
        indoc! {"
            1163751742
            1381373672
            2136511328
            3694931569
            7463417111
            1319128137
            1359912421
            3125421639
            1293138521
            2311944581
        "}
        .lines()
        .map(|row| row.chars().map(|c| c.to_digit(10).unwrap()).collect())
        .collect()
    }

    #[rstest]
    fn test_example(risk_levels: Vec<Vec<u32>>) {
        assert_eq!(Cave::grow(risk_levels).lowest_risk(), 315)
    }
}
