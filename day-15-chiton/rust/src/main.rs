use clap::{App, Arg};
use std::fs::read_to_string;
use chiton::Cave;

fn main() {
    let matches = App::new("chiton")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let risk_levels = read_to_string(matches.value_of("input").unwrap()).unwrap()
        .lines()
        .map(|line| line.chars().map(|c| c.to_digit(10).unwrap()).collect())
        .collect();

    let cave = Cave::grow(risk_levels);
    println!("{}", cave.lowest_risk());
}
