import click
from chiton import Cave


@click.command()
@click.argument("file", type=click.File("r"))
def main(file):
    risk_levels = [
        [int(c) for c in row]
        for row in file.read().splitlines()
    ]
    print(Cave(risk_levels).lowest_risk())
