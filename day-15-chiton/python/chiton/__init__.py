from typing import Tuple
from queue import PriorityQueue


class Cave:
    def __init__(self, risk_levels: list[list[int]]):
        self.risk_levels = [
            [
                (c - 1 + i + j) % 9 + 1
                for i in range(5)
                for c in row
            ]
            for j in range(5)
            for row in risk_levels
        ]
        self.height = len(self.risk_levels)
        self.width = len(self.risk_levels[0])

    def neighbors(self, p: Tuple[int, int]) -> list[Tuple[int, int]]:
        x, y = p
        return {
            (i, j)
            for (i, j) in [(x+1, y), (x, y+1), (x-1, y), (x, y-1)]
            if i in range(0, self.width)
            and j in range(0, self.height)
        }

    def lowest_risk(self) -> int:
        visited = set()
        dist = {}
        source = (0, 0)
        q = PriorityQueue()
        q.put((0, source))
        target = (self.width - 1, self.height - 1)

        while q:
            w, u = q.get()

            if u == target:
                return w

            for v in self.neighbors(u) - visited:
                alt = w + self.risk_levels[v[1]][v[0]]
                if dist.get(v) is None or alt < dist[v]:
                    dist[v] = alt
                    q.put((alt, v))
            visited.add(u)
