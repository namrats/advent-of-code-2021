import pytest
from inspect import cleandoc
from chiton import Cave


@pytest.fixture
def risk_levels():
    return [
        [int(c) for c in row]
        for row in
        cleandoc("""
            1163751742
            1381373672
            2136511328
            3694931569
            7463417111
            1319128137
            1359912421
            3125421639
            1293138521
            2311944581
        """).splitlines()
    ]


def test_example(risk_levels):
    assert Cave(risk_levels).lowest_risk() == 315
