mod parser;

#[derive(Debug, PartialEq)]
pub enum Command {
    Forward(i32),
    Down(i32),
    Up(i32),
}

impl Command {
    pub fn parse(input: &str) -> Option<Vec<Self>> {
        parser::commands(input).ok()
    }
}

use Command::*;

pub struct Submarine {
    aim: i32,
    depth: i32,
    horizontal: i32,
}

impl Submarine {
    pub fn new() -> Self {
        Self { aim: 0, depth: 0, horizontal: 0 }
    }

    pub fn execute(&mut self, command: Command) {
        match command {
            Up(distance) => self.aim -= distance,
            Down(distance) => self.aim += distance,
            Forward(distance) => {
                self.horizontal += distance;
                self.depth += self.aim * distance;
            }
        }
    }

    pub fn depth(&self) -> i32 {
        self.depth
    }

    pub fn horizontal(&self) -> i32 {
        self.horizontal
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn example() {
        let mut submarine = Submarine::new();
        let commands = vec![
            Forward(5),
            Down(5),
            Forward(8),
            Up(3),
            Down(8),
            Forward(2),
        ];
        for command in commands {
            submarine.execute(command);
        }

        assert_eq!(submarine.depth(), 60);
        assert_eq!(submarine.horizontal, 15);
    }
}
