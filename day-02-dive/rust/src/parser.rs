use nom::{
    self,
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, i32},
    combinator::map,
    error,
    multi::separated_list0,
    sequence::tuple,
    IResult,
    Finish,
};
use crate::Command;

#[derive(Debug)]
pub struct ParseError<'a>(error::Error<&'a str>);

macro_rules! enum_variant {
    ($variant_name:ident => $variant:ident) => {
        fn $variant_name(input: &str) -> IResult<&str, Command> {
            map(
                tuple((tag(stringify!($variant_name)), char(' '), i32)),
                |x| Command::$variant(x.2))(input)
        }
    };
}

enum_variant!(forward => Forward);
enum_variant!(up => Up);
enum_variant!(down => Down);

fn command(input: &str) -> IResult<&str, Command> {
    alt((forward, up, down))(input)
}

pub fn commands<'a>(input: &'a str) -> Result<Vec<Command>, ParseError<'a>> {
    separated_list0(tag("\n"), command)(input).finish().map(|(_, results)| results).map_err(|e| ParseError(e))
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;
    use Command::*;

    #[test]
    fn example() {
        let input = indoc! {"
            forward 5
            down 5
            forward 8
            up 3
            down 8
            forward 2
        "};

        let expected = vec![
            Forward(5),
            Down(5),
            Forward(8),
            Up(3),
            Down(8),
            Forward(2),
        ];

        assert_eq!(commands(input).unwrap(), expected);
    }
}
