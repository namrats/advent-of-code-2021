use dive::{Submarine, Command};
use clap::{App, Arg};
use std::fs::read_to_string;

fn main() {
    let matches = App::new("dive")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let commands = Command::parse(&read_to_string(matches.value_of("input").unwrap()).unwrap()).unwrap();

    let mut submarine = Submarine::new();
    for command in commands {
        submarine.execute(command);
    }

    println!("{}", submarine.depth() * submarine.horizontal());
}
