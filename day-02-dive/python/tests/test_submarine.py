from dive.submarine import Direction, Submarine


def test_example():
    submarine = Submarine()
    submarine.step(Direction.FORWARD, 5)
    submarine.step(Direction.DOWN, 5)
    submarine.step(Direction.FORWARD, 8)
    submarine.step(Direction.UP, 3)
    submarine.step(Direction.DOWN, 8)
    submarine.step(Direction.FORWARD, 2)

    assert submarine.depth == 60
    assert submarine.horizontal == 15
