import click
from .submarine import Direction, Submarine


@click.command()
@click.argument('file', type=click.File('r'))
def main(file):
    submarine = Submarine()
    for line in file:
        (direction, distance) = tuple(line.split())
        submarine.step(Direction[direction.upper()], int(distance))

    print(submarine.depth * submarine.horizontal)
