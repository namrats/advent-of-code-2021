from enum import Enum


class Direction(Enum):
    FORWARD = (1, 0)
    DOWN = (0, 1)
    UP = (0, -1)


class Submarine:
    def __init__(self):
        self.aim = 0
        self.depth = 0
        self.horizontal = 0

    def step(self, direction: Direction, distance: int):
        self.aim += direction.value[1] * distance

        self.horizontal += direction.value[0] * distance
        self.depth += direction.value[0] * self.aim * distance
