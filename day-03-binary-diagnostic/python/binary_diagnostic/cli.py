import click
from . import power_consumption, life_support


@click.group()
def main():
    pass


@main.command("power-consumption")
@click.argument('file', type=click.File('r'))
def power_consumption_command(file):
    print(power_consumption(list(file.read().splitlines())))


@main.command("life-support")
@click.argument('file', type=click.File('r'))
def life_support_command(file):
    print(life_support(list(file.read().splitlines())))
