from collections import Counter


def power_consumption(readings: list[str]) -> int:
    gamma, epsilon = (
        int("".join(bits), 2)
        for bits in zip(
            *(
                tuple(bit[0] for bit in Counter(bits).most_common())
                for bits in zip(*readings)
            )
        )
    )

    return gamma * epsilon


def life_support(readings: list[str]) -> int:
    o2_numbers = list(readings)
    co2_numbers = list(readings)
    base = Counter(["1", "0"])

    for i in range(len(readings[0])):
        bits = (o2_number[i] for o2_number in o2_numbers)
        most_common = (base + Counter(bits)).most_common()[0][0]
        o2_numbers = [o2_number for o2_number in o2_numbers if o2_number[i] == most_common]
        if len(o2_numbers) == 1:
            break

    for i in range(len(readings[0])):
        bits = (co2_number[i] for co2_number in co2_numbers)
        least_common = (base + Counter(bits)).most_common()[1][0]
        co2_numbers = [co2_number for co2_number in co2_numbers if co2_number[i] == least_common]
        if len(co2_numbers) == 1:
            break

    o2 = int("".join(o2_numbers[0]), 2)
    co2 = int("".join(co2_numbers[0]), 2)

    return o2 * co2
