use itertools::Itertools;
use std::cmp::Reverse;

pub fn power_consumption(readings: &[String]) -> u32 {
    let gamma = u32::from_str_radix(&(0..readings[0].len()).map(|i| most_common(readings, i).next().unwrap()).collect::<String>(), 2).unwrap();
    let epsilon = u32::from_str_radix(&(0..readings[0].len()).map(|i| most_common(readings, i).last().unwrap()).collect::<String>(), 2).unwrap();

    gamma * epsilon
}

pub fn life_support(readings: &[String]) -> u32 {
    let mut o2_numbers = readings.to_vec();

    for i in 0..readings[0].len() {
        let bit = most_common(&o2_numbers, i).next().unwrap();
        o2_numbers = o2_numbers.into_iter().filter(|n| n.chars().nth(i).unwrap() == bit).collect();
        if o2_numbers.len() == 1 {
            break;
        }
    }
    let o2 = u32::from_str_radix(&o2_numbers[0], 2).unwrap();

    let mut co2_numbers = readings.to_vec();
    for i in 0..readings[0].len() {
        let bit = most_common(&co2_numbers, i).last().unwrap();
        co2_numbers = co2_numbers.into_iter().filter(|n| n.chars().nth(i).unwrap() == bit).collect();
        if co2_numbers.len() == 1 {
            break;
        }
    }
    let co2 = u32::from_str_radix(&co2_numbers[0], 2).unwrap();

    o2 * co2
}

fn most_common(readings: &[String], i: usize) -> impl Iterator<Item=char> {
    readings.iter().map(|row| row.chars().nth(i).unwrap()).counts().into_iter().sorted_by_key(|&(i, c)| Reverse((c, i))).map(|(i, _)| i)
}

#[cfg(test)]
mod tests {
    use super::*;

    fn readings() -> Vec<String> {
        vec![
            "00100".into(),
            "11110".into(),
            "10110".into(),
            "10111".into(),
            "10101".into(),
            "01111".into(),
            "00111".into(),
            "11100".into(),
            "10000".into(),
            "11001".into(),
            "00010".into(),
            "01010".into(),
        ]
    }

    #[test]
    fn test_power_consumption() {
        assert_eq!(power_consumption(&readings()), 198);
    }

    #[test]
    fn test_life_support() {
        assert_eq!(life_support(&readings()), 230);
    }
}
