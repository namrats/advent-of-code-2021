use binary_diagnostic::{life_support, power_consumption};
use clap::{App, Arg, ArgMatches, SubCommand};
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let matches = App::new("binary_diagnostic")
        .subcommand(
            SubCommand::with_name("power-consumption")
            .arg(Arg::with_name("input").required(true))
        )
        .subcommand(
            SubCommand::with_name("life-support")
            .arg(Arg::with_name("input").required(true))
        )
        .get_matches();
    match matches.subcommand() {
        ("power-consumption", Some(matches)) => println!("{}", power_consumption(&input(matches))),
        ("life-support", Some(matches)) => println!("{}", life_support(&input(matches))),
        _ => unreachable!(),
    }
}

fn input(matches: &ArgMatches) -> Vec<String> {
    BufReader::new(File::open(matches.value_of("input").unwrap()).unwrap())
        .lines()
        .map(|line| line.unwrap())
        .collect()
}
