use clap::{App, Arg};
use hydrothermal_venture::VentDiagram;
use itertools::Itertools;
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let matches = App::new("hydrothermal-venture")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let coordinate_ranges = BufReader::new(File::open(matches.value_of("input").unwrap()).unwrap())
        .lines()
        .map(|line| {
            line.unwrap()
                .split(" -> ")
                .map(|p| {
                    p.split(",")
                        .map(|i| i.parse::<usize>().unwrap())
                        .collect_tuple()
                        .unwrap()
                })
                .collect_tuple()
                .unwrap()
        })
        .collect();
    let diagram = VentDiagram::new(coordinate_ranges);
    println!("{}", diagram.danger_point_count());
}
