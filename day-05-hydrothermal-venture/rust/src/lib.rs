use itertools::Itertools;
use std::collections::HashMap;
use std::iter::repeat;

pub type Point = (usize, usize);

pub struct VentDiagram {
    points_count: HashMap<Point, usize>,
}

fn genrange(a: usize, b: usize) -> Vec<usize> {
    if a < b {
        (a..=b).collect()
    } else {
        (b..=a).rev().collect()
    }
}

fn points_in_line(p1: Point, p2: Point) -> Vec<Point> {
    if p1.0 == p2.0 {
        repeat(p1.0).zip(genrange(p1.1, p2.1)).collect()
    } else if p1.1 == p2.1 {
        genrange(p1.0, p2.0).into_iter().zip(repeat(p1.1)).collect()
    } else {
        genrange(p1.0, p2.0)
            .into_iter()
            .zip(genrange(p1.1, p2.1))
            .collect()
    }
}

impl VentDiagram {
    pub fn new(coordinate_ranges: Vec<(Point, Point)>) -> Self {
        Self {
            points_count: coordinate_ranges
                .into_iter()
                .flat_map(|(p1, p2)| points_in_line(p1, p2).into_iter())
                .counts(),
        }
    }

    pub fn danger_point_count(&self) -> usize {
        self.points_count
            .values()
            .filter(|count| **count > 1)
            .count()
    }
}

impl ToString for VentDiagram {
    fn to_string(&self) -> String {
        let width = self.points_count.keys().map(|(x, _)| x).max().unwrap() + 1;
        let height = self.points_count.keys().map(|(_, y)| y).max().unwrap() + 1;

        (0..height)
            .map(|y| {
                (0..width)
                    .map(|x| {
                        self.points_count
                            .get(&(x, y))
                            .map(ToString::to_string)
                            .unwrap_or(".".into())
                    })
                    .collect::<String>()
            })
            .join("\n")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use indoc::indoc;

    #[test]
    fn test_example() {
        let coordinate_ranges = vec![
            ((0, 9), (5, 9)),
            ((8, 0), (0, 8)),
            ((9, 4), (3, 4)),
            ((2, 2), (2, 1)),
            ((7, 0), (7, 4)),
            ((6, 4), (2, 0)),
            ((0, 9), (2, 9)),
            ((3, 4), (1, 4)),
            ((0, 0), (8, 8)),
            ((5, 5), (8, 2)),
        ];

        let diagram = VentDiagram::new(coordinate_ranges);

        let expected = indoc!("
            1.1....11.
            .111...2..
            ..2.1.111.
            ...1.2.2..
            .112313211
            ...1.2....
            ..1...1...
            .1.....1..
            1.......1.
            222111....");

        assert_eq!(diagram.to_string(), expected);
    }
}
