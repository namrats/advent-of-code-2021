from collections import Counter
from itertools import repeat
from math import copysign

Point = (int, int)


class VentDiagram:
    def __init__(self, coordinate_ranges):
        self.point_count = Counter(
            p for p1, p2 in coordinate_ranges for p in points_in_line(p1, p2)
        )

    def danger_points(self):
        return [point for point, count in self.point_count.items() if count > 1]

    def __str__(self):
        width = max(x for x, _ in self.point_count) + 1
        height = max(y for _, y in self.point_count) + 1

        return "\n".join(
            "".join(
                str(self.point_count[(x, y)]).replace("0", ".") for x in range(width)
            )
            for y in range(height)
        )


def points_in_line(p1: Point, p2: Point) -> list[Point]:
    x1, y1 = p1
    x2, y2 = p2
    xs = repeat(x1) if x1 == x2 else genrange(x1, x2)
    ys = repeat(y1) if y1 == y2 else genrange(y1, y2)
    return zip(xs, ys)


def genrange(a: int, b: int):
    s = int(copysign(1, b - a))
    return range(a, b + s, s)
