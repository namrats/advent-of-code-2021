import click
from . import VentDiagram


@click.command()
@click.argument("file", type=click.File("r"))
def main(file):
    coordinate_ranges = [
        tuple(tuple(map(int, p.split(","))) for p in line.split(" -> "))
        for line in file.read().splitlines()
    ]
    diagram = VentDiagram(coordinate_ranges)
    print(len(diagram.danger_points()))
