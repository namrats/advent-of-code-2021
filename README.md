# Advent of Code 2021

My solutions to [Advent of Code 2021](https://adventofcode.com/2021) in various programming languages.

## Build and run

### Python

Requirements:

- Python 3.9.5
- Poetry 1.1.12

Each project provides a binary that has the same name as the project.

```shell
poetry install
poetry run <project-name> ../input.txt
```

### Rust

Requirements:

- Rust, Cargo 1.57.0
