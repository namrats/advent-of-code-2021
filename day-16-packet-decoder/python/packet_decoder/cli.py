import click
from bitstring import ConstBitStream
from packet_decoder import Packet


@click.command()
@click.argument("file", type=click.File("r"))
def main(file):
    packet = Packet(ConstBitStream(hex=file.read()))
    print(packet.value())
