from enum import IntEnum
from functools import reduce
from operator import mul


class PacketType(IntEnum):
    SUM = 0
    PRODUCT = 1
    MINIMUM = 2
    MAXIMUM = 3
    LITERAL = 4
    GREATER = 5
    LESS = 6
    EQUAL = 7


class Packet:
    def __init__(self, bits):
        self.version = bits.read("uint:3")
        self.type_id = bits.read("uint:3")

        self.subpackets = []
        self.number = 0

        if self.type_id == 4:
            while True:
                finish = not bits.read("bool")
                self.number = self.number * 16 + bits.read("uint:4")
                if finish:
                    break
        elif bits.read("bool"):
            subpackets_count = bits.read("uint:11")
            for _ in range(subpackets_count):
                self.subpackets.append(self.__class__(bits))
        else:
            subpackets_length = bits.read("uint:15")
            subpacket_bits = bits.read(f"bits:{subpackets_length}")
            while subpacket_bits.pos < subpackets_length - 1:
                self.subpackets.append(self.__class__(subpacket_bits))

    def version_sum(self) -> int:
        return self.version + sum(packet.version_sum() for packet in self.subpackets)

    def value(self) -> int:
        if self.type_id == PacketType.LITERAL:
            return self.number
        elif self.type_id == PacketType.SUM:
            return sum(packet.value() for packet in self.subpackets)
        elif self.type_id == PacketType.PRODUCT:
            values = (packet.value() for packet in self.subpackets)
            return reduce(mul, values, 1)
        elif self.type_id == PacketType.MINIMUM:
            return min(packet.value() for packet in self.subpackets)
        elif self.type_id == PacketType.MAXIMUM:
            return max(packet.value() for packet in self.subpackets)
        elif self.type_id == PacketType.GREATER:
            return int(self.subpackets[0].value() > self.subpackets[1].value())
        elif self.type_id == PacketType.LESS:
            return int(self.subpackets[0].value() < self.subpackets[1].value())
        elif self.type_id == PacketType.EQUAL:
            return int(self.subpackets[0].value() == self.subpackets[1].value())

    def __str__(self):
        return f"Packet({self.version=}, {self.type_id=}, {self.number=}, subpackets={list(map(str, self.subpackets))})"
