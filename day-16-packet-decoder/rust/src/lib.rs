use bitstream_io::{BitRead, BitReader, BigEndian};
use std::io::Read;

pub enum Operator {
    Sum, Product, Min, Max, Greater, Less, Equal
}

impl Operator {
    pub fn apply(&self, values: Vec<u64>) -> u64 {
        match self {
            &Self::Sum => values.into_iter().sum(),
            &Self::Product => values.into_iter().product(),
            &Self::Min => values.into_iter().min().unwrap(),
            &Self::Max => values.into_iter().max().unwrap(),
            &Self::Greater => (values[0] > values[1]) as u64,
            &Self::Less => (values[0] < values[1]) as u64,
            &Self::Equal => (values[0] == values[1]) as u64,
        }
    }
}

pub enum Packet {
    Operator { operator: Operator, version: u32, packets: Vec<Packet> },
    Literal { version: u32, number: u64 },
}

impl Packet {
    pub fn new<R: Read>(bits: &mut BitReader<R, BigEndian>) -> (u32, Self) {
        let version: u32 = bits.read(3).unwrap();
        let type_id: u8 = bits.read(3).unwrap();
        let mut read_bit_count = 6;

        if type_id == 4 {
            let mut number: u64 = 0;
            loop {
                let finish = !bits.read_bit().unwrap();
                number = number * 16 + bits.read::<u64>(4).unwrap();
                read_bit_count += 5;
                if finish {
                    break;
                }
            }
            (read_bit_count, Self::Literal { version, number })
        } else {
            let packets = if bits.read_bit().unwrap() {
                let subpackets_count = bits.read(11).unwrap();
                read_bit_count += 11;
                (0..subpackets_count).map(|_| {
                    let (bit_count, packet) = Self::new(bits);
                    read_bit_count += bit_count;
                    packet
                }).collect()
            } else {
                let mut subpackets_length: u32 = bits.read(15).unwrap();
                read_bit_count += 15;
                let mut packets = Vec::new();
                while subpackets_length > 0 {
                    let (bit_count, packet) = Self::new(bits);
                    subpackets_length -= bit_count;
                    read_bit_count += bit_count;
                    packets.push(packet);
                }
                packets
            };
            read_bit_count += 1;

            let operator = match type_id {
                0 => Operator::Sum,
                1 => Operator::Product,
                2 => Operator::Min,
                3 => Operator::Max,
                5 => Operator::Greater,
                6 => Operator::Less,
                7 => Operator::Equal,
                _ => unreachable!(),
            };

            (read_bit_count, Self::Operator { version, operator, packets })
        }
    }

    pub fn version_sum(&self) -> u32 {
        match self {
            Self::Literal { version, .. } => *version,
            Self::Operator { version, packets, .. } => *version + packets.iter().map(|packet| packet.version_sum()).sum::<u32>(),
        }
    }

    pub fn value(&self) -> u64 {
        match self {
            Self::Literal { number, .. } => *number,
            Self::Operator { operator, packets, .. } => operator.apply(packets.iter().map(|packet| packet.value()).collect())
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::rstest;
    use bitstream_io::{BitReader, BigEndian};
    use hex::decode;

    #[rstest]
    #[case("D2FE28", 6)]
    #[case("38006F45291200", 9)]
    #[case("EE00D40C823060", 14)]
    #[case("8A004A801A8002F478", 16)]
    #[case("620080001611562C8802118E34", 12)]
    #[case("C0015000016115A2E0802F182340", 23)]
    #[case("A0016C880162017C3686B18A3D4780", 31)]
    fn test_version_sum(#[case] text: &str, #[case] expected: u32) {
        let decoded = decode(text).unwrap();
        let mut bits = BitReader::<_, BigEndian>::new(decoded.as_slice());
        let (_, packet) = Packet::new(&mut bits);
        assert_eq!(packet.version_sum(), expected);
    }
}
