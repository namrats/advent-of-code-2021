use clap::{App, Arg};
use std::fs::read_to_string;
use hex::decode;
use bitstream_io::{BitReader, BigEndian};
use packet_decoder::Packet;

fn main() {
    let matches = App::new("chiton")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let content = decode(read_to_string(matches.value_of("input").unwrap()).unwrap().trim_end()).unwrap();
    let mut bits = BitReader::endian(content.as_slice(), BigEndian);
    let (_, packet) = Packet::new(&mut bits);
    println!("{}", packet.value());
}
