use std::fs::read_to_string;
use clap::{App, Arg};
use itertools::Itertools;
use transparent_origami::{OrigamiPaper, Fold};

fn main() {
    let matches = App::new("passage-pathing")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let content = read_to_string(matches.value_of("input").unwrap()).unwrap();
    let (dots, folds) = content.split_once("\n\n").unwrap();
    let dots = dots
        .lines()
        .map(|line| line.split_once(",").unwrap())
        .map(|(x, y)| (x.parse::<usize>().unwrap(), y.parse::<usize>().unwrap()))
        .collect_vec();
    let folds = folds.lines().map(|fold| fold.parse::<Fold>().unwrap());
    let mut paper = OrigamiPaper::new(dots);

    for fold in folds {
        paper.fold(fold.direction, fold.n);
    }

    println!("{}", paper.to_string());
}
