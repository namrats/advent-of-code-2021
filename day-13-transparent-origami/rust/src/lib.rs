mod parser;

use std::collections::HashSet;
use itertools::Itertools;
pub use parser::*;

pub struct Fold {
    pub direction: FoldDirection,
    pub n: usize,
}

pub enum FoldDirection { X, Y }

impl FoldDirection {
    fn project(&self, dot: (usize, usize)) -> usize {
        match self {
            FoldDirection::X => dot.0,
            FoldDirection::Y => dot.1,
        }
    }

    fn project_mut<'a>(&'_ self, dot: &'a mut (usize, usize)) -> &'a mut usize {
        match self {
            FoldDirection::X => &mut dot.0,
            FoldDirection::Y => &mut dot.1,
        }
    }
}

pub struct OrigamiPaper {
    dots: HashSet<(usize, usize)>,
    dimensions: (usize, usize),
}

impl OrigamiPaper {
    pub fn new<I>(dots: I) -> Self
    where I: IntoIterator<Item=(usize, usize)>
    {
        let dots: HashSet<_> = dots.into_iter().collect();
        let dimensions = (dots.iter().map(|(i, _)| i+1).max().unwrap(), dots.iter().map(|(_, j)| j+1).max().unwrap());
        Self {
            dots,
            dimensions
        }
    }

    pub fn fold(&mut self, d: FoldDirection, n: usize) {
        let mut folded_dots: Vec<_> = self.dots.iter().filter(|&&dot| d.project(dot) > n).cloned().collect();
        self.dots.retain(|&dot| d.project(dot) < n);
        folded_dots.iter_mut().map(|dot| d.project_mut(dot)).for_each(|c| *c = 2 * n - *c);
        self.dots.extend(folded_dots);

        let dim = d.project_mut(&mut self.dimensions);
        *dim = n;
    }

    pub fn dots(&self) -> impl Iterator<Item=&(usize, usize)> {
        self.dots.iter()
    }
}

impl ToString for OrigamiPaper {
    fn to_string(&self) -> String {
        (0..self.dimensions.1).map(|j| {
            (0..self.dimensions.0).map(|i| {
                if self.dots.contains(&(i, j)) {
                    '#'
                } else {
                    '.'
                }
            }).collect::<String>()
        }).join("\n")
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::{fixture, rstest};
    use indoc::indoc;

    fn dots_from_str(s: &str) -> HashSet<(usize, usize)> {
        s.lines().enumerate()
            .flat_map(|(j, line)| line.chars().enumerate().map(move |(i, c)| ((i, j), c)))
            .filter(|(_, c)| *c == '#')
            .map(|(dot, _)| dot)
            .collect()
    }

    #[fixture]
    fn dots() -> Vec<(usize, usize)> {
        vec![
        (6, 10),
        (0, 14),
        (9, 10),
        (0, 3),
        (10, 4),
        (4, 11),
        (6, 0),
        (6, 12),
        (4, 1),
        (0, 13),
        (10, 12),
        (3, 4),
        (3, 0),
        (8, 4),
        (1, 10),
        (2, 14),
        (8, 10),
        (9, 0),
        ]
    }

    #[rstest]
    fn test_example(dots: Vec<(usize, usize)>) {
        let mut paper = OrigamiPaper::new(dots);
        paper.fold(FoldDirection::Y, 7);
        paper.fold(FoldDirection::X, 5);

        let expected = dots_from_str(indoc! {"
            #####
            #...#
            #...#
            #...#
            #####
            .....
            .....
        "});

        assert_eq!(paper.dots().cloned().collect::<HashSet<_>>(), expected);
    }
}
