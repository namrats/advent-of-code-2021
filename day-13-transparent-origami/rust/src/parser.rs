use std::str::FromStr;
use nom::{
    self,
    branch::alt,
    bytes::complete::tag,
    character::complete::{char, u32},
    combinator::map,
    sequence::tuple,
    IResult,
    Finish,
};
use super::{Fold, FoldDirection};

#[derive(Debug)]
pub struct ParseError(String);

fn fold_direction(input: &str) -> IResult<&str, FoldDirection> {
    alt((
        map(tag("x"), |_| FoldDirection::X),
        map(tag("y"), |_| FoldDirection::Y),
    ))(input)
}

fn fold(input: &str) -> IResult<&str, Fold> {
    map(tuple((
        tag("fold along"),
        char(' '),
        fold_direction,
        char('='),
        u32)),
        |x| Fold { direction: x.2, n: x.4 as usize })(input)
}

impl FromStr for Fold {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        fold(s).finish().map(|(_, result)| result).map_err(|e| ParseError(format!("{}", e)))
    }
}
