from enum import Enum
from typing import Tuple, Iterable
from operator import itemgetter


class FoldDirection(Enum):
    X = itemgetter(0)
    Y = itemgetter(1)

    def project(self, dot: Tuple[int, int]) -> int:
        return self.value(dot)

    def fold(self, dot: Tuple[int, int], n: int) -> Tuple[int, int]:
        x, y = dot
        if self is self.X:
            return (2 * n - x, y)
        else:
            return (x, 2 * n - y)


class OrigamiPaper:
    def __init__(self, dots: Iterable[Tuple[int, int]]):
        self.dots = set(dots)
        self.dimensions = tuple(max(c) + 1 for c in zip(*self.dots))

    def fold(self, direction: FoldDirection, n: int) -> None:
        folded_dots = {dot for dot in self.dots if direction.project(dot) > n}
        self.dots -= folded_dots
        folded_dots = {direction.fold(dot, n) for dot in folded_dots}
        self.dots |= folded_dots

        if direction is FoldDirection.X:
            self.dimensions = (n, self.dimensions[1])
        else:
            self.dimensions = (self.dimensions[0], n)

    def __str__(self):
        return "\n".join(
            "".join(
                "#" if (i, j) in self.dots else "." for i in range(self.dimensions[0])
            )
            for j in range(self.dimensions[1])
        )
