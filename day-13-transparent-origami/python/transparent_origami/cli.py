import click
import re
from . import OrigamiPaper, FoldDirection


@click.command()
@click.argument("file", type=click.File("r"))
def main(file):
    dots, folds = file.read().split("\n\n")
    paper = OrigamiPaper(
        tuple(int(c) for c in line.split(",")) for line in dots.splitlines()
    )

    for fold in folds.splitlines():
        m = re.fullmatch(r"fold along (\w)=(\d+)", fold)
        direction = FoldDirection[m.group(1).upper()]
        n = int(m.group(2))

        paper.fold(direction, n)

    print(paper)
