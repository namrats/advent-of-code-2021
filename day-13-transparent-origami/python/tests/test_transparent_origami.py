import pytest
from inspect import cleandoc
from transparent_origami import OrigamiPaper, FoldDirection


@pytest.fixture
def dots():
    return [
        (6, 10),
        (0, 14),
        (9, 10),
        (0, 3),
        (10, 4),
        (4, 11),
        (6, 0),
        (6, 12),
        (4, 1),
        (0, 13),
        (10, 12),
        (3, 4),
        (3, 0),
        (8, 4),
        (1, 10),
        (2, 14),
        (8, 10),
        (9, 0),
    ]


def test_example(dots):
    paper = OrigamiPaper(dots)
    paper.fold(FoldDirection.Y, 7)
    paper.fold(FoldDirection.X, 5)

    expected = cleandoc(
        """
        #####
        #...#
        #...#
        #...#
        #####
        .....
        .....
    """
    )

    assert str(paper) == expected
