module SyntaxScoringSpec (spec) where

import Data.Foldable (for_)
import Test.Hspec
import SyntaxScoring (errorScore, parseLine, autocomplete, autocompleteScore)

spec :: Spec
spec =
  describe "SyntaxScoring" $ do
    describe "errorScore" $
      let
        test (input, expected) =
          let assertion = errorScore input `shouldBe` expected
          in it input assertion
        cases =
          [ ("[({(<(())[]>[[{[]{<()<>>", 0)
          , ("[(()[<>])]({[<{<<[]>>(", 0)
          , ("{([(<{}[<>[]}>{[]{[(<()>", 1197)
          , ("(((({<>}<{<{<>}{[]{[]{}", 0)
          , ("[[<[([]))<([[{}[[()]]]", 3)
          , ("[{[{({}]{}}([{[{{{}}([]", 57)
          , ("{<[[]]>}<{[{[{[]{()[[[]", 0)
          , ("[<(<(<(<{}))><([]([]()", 3)
          , ("<{([([[(<>()){}]>(<<{{", 25137)
          , ("<{([{{}}[<[[[<>{}]]]>[]]", 0)
          ]
      in for_ cases test
    describe "autocomplete" $
      let
        test (input, expected) =
          let
            assertion = autocompletion `shouldBe` Just expected
            autocompletion =
              case parseLine input of
                Right (_, Just i) -> Just $ autocomplete i
                _ -> Nothing
          in it input assertion
        cases =
          [ ("[({(<(())[]>[[{[]{<()<>>", "}}]])})]")
          , ("[(()[<>])]({[<{<<[]>>(", ")}>]})")
          , ("(((({<>}<{<{<>}{[]{[]{}", "}}>}>))))")
          , ("{<[[]]>}<{[{[{[]{()[[[]", "]]}}]}]}>")
          , ("<{([{{}}[<[[[<>{}]]]>[]]", "])}>")
          ]
        in for_ cases test
    describe "autocompleteScore" $
      let
        test (input, expected) =
          let
            assertion = autocompleteScore input `shouldBe` Just expected
          in it input assertion
        cases =
          [ ("[({(<(())[]>[[{[]{<()<>>", 288957)
          , ("[(()[<>])]({[<{<<[]>>(", 5566)
          , ("(((({<>}<{<{<>}{[]{[]{}", 1480781)
          , ("{<[[]]>}<{[{[{[]{()[[[]", 995444)
          , ("<{([{{}}[<[[[<>{}]]]>[]]", 294)
          ]
        in for_ cases test
