module SyntaxScoring
    ( errorScore
    , parseLine
    , autocomplete
    , autocompleteScore
    ) where

import Text.ParserCombinators.Parsec
import Text.Parsec.Error (ParseError, Message(..), errorMessages, messageString)

errorScore :: String -> Integer
errorScore input =
  case parseLine input of
    Left err -> score (errorMessages err)
    Right _ -> 0
  where
    score msgs
      | not (null sysUnExpect) = case messageString (head sysUnExpect) of
            "\")\"" -> 3
            "\"]\"" -> 57
            "\"}\"" -> 1197
            "\">\"" -> 25137
            _ -> 0
      | otherwise = 0
      where
        sysUnExpect = filter (SysUnExpect "" ==) msgs

data Chunk = Chunk String String [Chunk]
  deriving Show

data IncompleteChunk = IncompleteChunk String [Chunk] (Maybe IncompleteChunk)
  deriving Show

delimiter :: String -> String -> Parser Chunk
delimiter o c = do
  _ <- string o
  chunks <- line
  _ <- string c
  return $ Chunk o c chunks

incompleteChunk :: String -> Parser IncompleteChunk
incompleteChunk o = do
  _ <- string o
  (chunks, i) <- incompleteLine
  return $ IncompleteChunk o chunks i

line :: Parser [Chunk]
line =
  many $ choice [delimiter "{" "}", delimiter "(" ")", delimiter "[" "]", delimiter "<" ">"]

incompleteLine :: Parser ([Chunk], Maybe IncompleteChunk)
incompleteLine =
  do
    hd <- try(choice [delimiter "{" "}", delimiter "(" ")", delimiter "[" "]", delimiter "<" ">"])
    (tl, i) <- incompleteLine
    return (hd:tl, i)
  <|> do
    i <- choice [incompleteChunk "{", incompleteChunk "(", incompleteChunk "[", incompleteChunk "<"]
    return ([], Just i)
  <|> return ([], Nothing)


parseLine :: String -> Either ParseError ([Chunk], Maybe IncompleteChunk)
parseLine = parse (incompleteLine <* eof) "(unknown)"

autocomplete :: IncompleteChunk -> String
autocomplete (IncompleteChunk d _ Nothing) = closer d
autocomplete (IncompleteChunk d _ (Just i)) = autocomplete i ++ closer d

closer :: String -> String
closer "(" = ")"
closer "[" = "]"
closer "{" = "}"
closer "<" = ">"

autocompleteScore :: String -> Maybe Integer
autocompleteScore input =
  case parseLine input of
    Right (_, Just i) -> Just $ foldl score 0 $ autocomplete i
    _ -> Nothing
  where
    score s ')' = 1 + 5 * s
    score s ']' = 2 + 5 * s
    score s '}' = 3 + 5 * s
    score s '>' = 4 + 5 * s
    score _ _ = 0
