module Main where

import Options.Applicative
import Control.Monad (join)
import Data.List (sort)
import Data.Maybe (mapMaybe)
import Data.Semigroup ((<>))
import System.IO
import SyntaxScoring

data ArgMatch
  = ErrorScore String
  | AutocompleteScore String

argMatch :: Parser (IO ())
argMatch =
  subparser
    ( command "error-score" (info (printErrorScore <$> argument str (metavar "FILE")) idm)
    <> command "autocomplete-score" (info (printAutocompleteScore <$> argument str (metavar "FILE")) idm)
    )

main :: IO ()
main = join $ execParser opts
  where
    opts = info (argMatch <**> helper) (progDesc "Syntax Scoring")

printErrorScore :: String -> IO ()
printErrorScore filename = do
  handle <- openFile filename ReadMode
  contents <- hGetContents handle
  print $ sum $ map errorScore $ lines contents
  hClose handle

printAutocompleteScore :: String -> IO ()
printAutocompleteScore filename = do
  handle <- openFile filename ReadMode
  contents <- hGetContents handle
  let scores = sort $ mapMaybe autocompleteScore $ lines contents
  print $ scores !! (length scores `div` 2)
  hClose handle
