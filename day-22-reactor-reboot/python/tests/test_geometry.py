import pytest
from reactor_reboot.geometry import Interval, Cuboid


class TestInterval:
    @pytest.mark.parametrize("i1,i2,expected", [
        ((0, 2), (1, 3), True),
        ((0, 2), (2, 3), True),
        ((0, 2), (3, 5), False),
        ((3, 5), (0, 2), False),
    ])
    def test_overlap(self, i1, i2, expected):
        assert Interval(*i1).overlap(Interval(*i2)) == expected

    @pytest.mark.parametrize("i,expected", [
        ((0, 1), 2),
        ((0, 0), 1),
        ((0, -1), 0),
    ])
    def test_length(self, i, expected):
        assert Interval(*i).length() == expected


class TestCuboid:
    @pytest.mark.parametrize("c1,c2,expected", [
        (
            ((0, 1), (0, 0), (0, 0)),
            ((1, 1), (0, 0), (0, 0)),
            [((0, 0), (0, 0), (0, 0))]
        )
    ])
    def test_sub(self, c1, c2, expected):
        actual = Cuboid.from_coords(c1) - Cuboid.from_coords(c2)
        expected = list(map(Cuboid.from_coords, expected))
        assert actual == expected

    def test_bool(self):
        assert not Cuboid.from_coords(((2, 1), (0, 0), (0, 0)))
