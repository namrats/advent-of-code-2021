import pytest
from pathlib import Path
from reactor_reboot import Reactor, RebootStep
from reactor_reboot.geometry import Cuboid


def load_reboot_steps(filename):
    with Path(__file__).with_name(filename).open() as file:
        return list(map(RebootStep.load, file.read().splitlines()))


class TestSmallExample:
    @pytest.fixture
    def steps(self):
        return load_reboot_steps("small_example.txt")

    def test_example(self, steps):
        reactor = Reactor()

        for step, expected in zip(steps, [27, 46, 38, 39]):
            reactor.execute(step)

            assert reactor.cubes_on_count() == expected


class TestMediumExample:
    @pytest.fixture
    def steps(self):
        return load_reboot_steps("medium_example.txt")

    def test_example(self, steps):
        reactor = Reactor(Cuboid.from_coords(((-50, 50), (-50, 50), (-50, 50))))

        for i, step in enumerate(steps):
            reactor.execute(step)

        assert reactor.cubes_on_count() == 590784


class TestLargeExample:
    @pytest.fixture
    def steps(self):
        return load_reboot_steps("large_example.txt")

    def test_example(self, steps):
        reactor = Reactor()

        for i, step in enumerate(steps):
            reactor.execute(step)

        assert reactor.cubes_on_count() == 2758514936282235
