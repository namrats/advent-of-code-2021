import re
from dataclasses import dataclass
from typing import ClassVar
from .geometry import Interval, Cuboid


@dataclass
class RebootStep:
    STEP_RE: ClassVar[re.Pattern] = re.compile(
        r"(on|off) x=(-?\d+)\.\.(-?\d+),y=(-?\d+)\.\.(-?\d+),z=(-?\d+)\.\.(-?\d+)")  # noqa: E501

    on: bool
    cuboid: Cuboid

    @classmethod
    def load(cls, text) -> "RebootStep":
        m = cls.STEP_RE.fullmatch(text)
        on = m.group(1) == "on"
        xs = Interval(int(m.group(2)), int(m.group(3)))
        ys = Interval(int(m.group(4)), int(m.group(5)))
        zs = Interval(int(m.group(6)), int(m.group(7)))
        return cls(on, Cuboid((xs, ys, zs)))


class Reactor:
    def __init__(self, bounds=None):
        self.cuboids = []
        self.bounds = bounds

    def execute(self, step: RebootStep):
        if not self.bounds or self.bounds.contains(step.cuboid):
            self.cuboids = [
                c
                for cuboid in self.cuboids
                for c in (cuboid - step.cuboid)
            ]
            if step.on:
                self.cuboids.append(step.cuboid)

    def cubes_on_count(self):
        return sum(c.volume() for c in self.cuboids)
