import click
from reactor_reboot import Reactor, RebootStep


@click.command()
@click.argument("file", type=click.File("r"))
def main(file):
    steps = map(RebootStep.load, file.read().splitlines())
    reactor = Reactor()
    for step in steps:
        reactor.execute(step)

    print(reactor.cubes_on_count())
