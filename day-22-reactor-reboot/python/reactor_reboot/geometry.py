from dataclasses import dataclass
from itertools import product
from functools import reduce
from operator import mul
from typing import Tuple


@dataclass
class Interval:
    lower: int
    upper: int

    def overlap(self, other: "Interval") -> bool:
        return self.lower <= other.upper and other.lower <= self.upper

    def contains(self, other: "Interval") -> bool:
        return self.lower <= other.lower and other.upper <= self.upper

    def __and__(self, other: "Interval") -> "Interval":
        if self.contains(other):
            return other
        elif other.contains(self):
            return self
        else:
            return Interval(
                max(self.lower, other.lower),
                min(self.upper, other.upper)
            )

    def length(self) -> int:
        return max(0, self.upper - self.lower + 1)

    def __bool__(self) -> bool:
        return self.lower <= self.upper


@dataclass
class Cuboid:
    sides: Tuple[Interval, Interval, Interval]

    @classmethod
    def from_coords(cls, coords) -> "Cuboid":
        return cls(tuple(Interval(*i) for i in coords))

    def overlap(self, other: "Cuboid") -> bool:
        return all(
            side.overlap(other_side)
            for side, other_side in zip(self.sides, other.sides)
        )

    def contains(self, other: "Cuboid") -> bool:
        return all(
            side.contains(other_side)
            for side, other_side in zip(self.sides, other.sides)
        )

    def __and__(self, other: "Cuboid") -> "Cuboid":
        if self.contains(other):
            return other
        elif other.contains(self):
            return self
        else:
            return Cuboid(tuple(
                side & other_side
                for side, other_side in zip(self.sides, other.sides)
            ))

    def __sub__(self, other: "Cuboid") -> list["Cuboid"]:
        if self.overlap(other):
            i = self & other

            cuboidlet_sides = product(*(
                    [
                        Interval(side.lower, i_side.lower - 1),
                        Interval(i_side.lower, i_side.upper),
                        Interval(i_side.upper+1, side.upper)
                    ]
                    for side, i_side in zip(self.sides, i.sides)
                ))

            result = [c for c in map(Cuboid, cuboidlet_sides) if c and c != i]
            return result

        else:
            return [self]

    def volume(self) -> int:
        return reduce(mul, (side.length() for side in self.sides), 1)

    def __bool__(self) -> bool:
        return all(self.sides)
