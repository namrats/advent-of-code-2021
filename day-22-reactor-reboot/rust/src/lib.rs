pub mod geometry;
mod parser;

use geometry::Cuboid;

#[derive(Debug)]
pub struct Reactor {
    cuboids: Vec<Cuboid>,
    bounds: Option<Cuboid>,
}

impl Reactor {
    pub fn new() -> Self {
        Self {
            cuboids: Vec::new(),
            bounds: None,
        }
    }

    pub fn new_with_bounds(bounds: Cuboid) -> Self {
        Self {
            cuboids: Vec::new(),
            bounds: Some(bounds),
        }
    }

    pub fn execute(&mut self, step: RebootStep) {
        if self
            .bounds
            .as_ref()
            .map(|b| b.contains(&step.cuboid))
            .unwrap_or(true)
        {
            self.cuboids = self
                .cuboids
                .iter()
                .flat_map(|cuboid| cuboid - &step.cuboid)
                .collect();
            if step.on {
                self.cuboids.push(step.cuboid);
            }
        }
    }

    pub fn cubes_on_count(&self) -> u64 {
        self.cuboids.iter().map(Cuboid::volume).sum()
    }
}

#[derive(Debug)]
pub struct RebootStep {
    on: bool,
    cuboid: Cuboid,
}

impl RebootStep {
    pub fn new(on: bool, cuboid: Cuboid) -> Self {
        Self { on, cuboid }
    }
}
