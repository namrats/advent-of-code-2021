use itertools::Itertools;
use std::cmp::{max, min};
use std::ops::{BitAnd, Sub};

#[derive(Clone, Debug, PartialEq)]
pub struct Interval {
    lower: i32,
    upper: i32,
}

impl Interval {
    pub fn new(lower: i32, upper: i32) -> Option<Self> {
        Some(Self { lower, upper }).filter(|i| i.lower <= i.upper)
    }

    pub fn length(&self) -> u64 {
        (self.upper - self.lower + 1) as u64
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        self.lower <= other.upper && other.lower <= self.upper
    }

    pub fn contains(&self, other: &Self) -> bool {
        self.lower <= other.lower && other.upper <= self.upper
    }
}

impl BitAnd for &Interval {
    type Output = Interval;

    fn bitand(self, rhs: Self) -> Self::Output {
        Interval {
            lower: max(self.lower, rhs.lower),
            upper: min(self.upper, rhs.upper),
        }
    }
}

#[derive(Clone, Debug, PartialEq)]
pub struct Cuboid {
    sides: Vec<Interval>,
}

impl Cuboid {
    pub fn from_coords(coords: ((i32, i32), (i32, i32), (i32, i32))) -> Option<Self> {
        Some(Self::new(vec![
            Interval::new(coords.0 .0, coords.0 .1)?,
            Interval::new(coords.1 .0, coords.1 .1)?,
            Interval::new(coords.2 .0, coords.2 .1)?,
        ]))
    }

    pub fn new(sides: Vec<Interval>) -> Self {
        Self { sides }
    }

    pub fn overlaps(&self, other: &Self) -> bool {
        self.sides
            .iter()
            .zip(other.sides.iter())
            .all(|(side, other_side)| side.overlaps(other_side))
    }

    pub fn contains(&self, other: &Self) -> bool {
        self.sides
            .iter()
            .zip(other.sides.iter())
            .all(|(side, other_side)| side.contains(other_side))
    }

    pub fn volume(&self) -> u64 {
        self.sides.iter().map(Interval::length).product()
    }
}

impl BitAnd for &Cuboid {
    type Output = Cuboid;

    fn bitand(self, rhs: Self) -> Self::Output {
        if self.contains(rhs) {
            rhs.clone()
        } else if rhs.contains(self) {
            self.clone()
        } else {
            let sides = self
                .sides
                .iter()
                .zip(rhs.sides.iter())
                .map(|(side, rhs_side)| side & rhs_side)
                .collect();
            Cuboid { sides }
        }
    }
}

impl Sub for &Cuboid {
    type Output = Vec<Cuboid>;

    fn sub(self, rhs: Self) -> Self::Output {
        if rhs.contains(self) {
            vec![]
        } else if self.overlaps(rhs) {
            let intersection = self & rhs;
            self.sides
                .iter()
                .zip(intersection.sides.iter())
                .map(|(side, i_side)| {
                    vec![
                        Interval::new(side.lower, i_side.lower - 1),
                        Interval::new(i_side.lower, i_side.upper),
                        Interval::new(i_side.upper + 1, side.upper),
                    ]
                })
                .multi_cartesian_product()
                .filter_map(|v| v.into_iter().collect())
                .map(Cuboid::new)
                .filter(|cuboid| cuboid != &intersection)
                .collect()
        } else {
            vec![self.clone()]
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_sub() {
        let c1 = Cuboid::from_coords(((0, 1), (0, 0), (0, 0))).unwrap();
        let c2 = Cuboid::from_coords(((1, 1), (0, 0), (0, 0))).unwrap();
        let expected = vec![Cuboid::from_coords(((0, 0), (0, 0), (0, 0))).unwrap()];

        assert_eq!(&c1 - &c2, expected);
    }
}
