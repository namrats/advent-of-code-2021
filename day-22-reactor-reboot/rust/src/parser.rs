use nom::{
    self, branch::alt, bytes::complete::tag, character::complete::i32, combinator::map,
    sequence::tuple, Finish, IResult,
};
use std::str::FromStr;

use super::geometry::{Cuboid, Interval};
use super::RebootStep;

#[derive(Debug)]
pub struct ParseError(String);

fn interval(input: &str) -> IResult<&str, Interval> {
    map(tuple((i32, tag(".."), i32)), |x| {
        Interval::new(x.0, x.2).unwrap()
    })(input)
}

fn reboot_step(input: &str) -> IResult<&str, RebootStep> {
    let (input, on) = alt((map(tag("on"), |_| true), map(tag("off"), |_| false)))(input)?;
    let (input, _) = tag(" x=")(input)?;
    let (input, xs) = interval(input)?;
    let (input, _) = tag(",y=")(input)?;
    let (input, ys) = interval(input)?;
    let (input, _) = tag(",z=")(input)?;
    let (input, zs) = interval(input)?;

    let cuboid = Cuboid::new(vec![xs, ys, zs]);
    let step = RebootStep::new(on, cuboid);
    Ok((input, step))
}

impl FromStr for RebootStep {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        reboot_step(s)
            .finish()
            .map(|(_, result)| result)
            .map_err(|e| ParseError(format!("{}", e)))
    }
}
