use clap::{App, Arg};
use reactor_reboot::{Reactor, RebootStep};
use std::fs::read_to_string;
use std::str::FromStr;

fn main() {
    let matches = App::new("reactor-reboot")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let content = read_to_string(matches.value_of("input").unwrap()).unwrap();
    let steps = content
        .lines()
        .map(RebootStep::from_str)
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    let mut reactor = Reactor::new();
    for step in steps {
        reactor.execute(step);
    }

    println!("{}", reactor.cubes_on_count());
}
