use reactor_reboot::{geometry::Cuboid, Reactor, RebootStep};
use std::str::FromStr;

#[test]
fn test_small_example() {
    let input = include_str!("small_example.txt");
    let steps = input
        .lines()
        .map(RebootStep::from_str)
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    let mut reactor = Reactor::new();

    for step in steps {
        reactor.execute(step);
    }
    assert_eq!(reactor.cubes_on_count(), 39);
}

#[test]
fn test_medium_example() {
    let input = include_str!("medium_example.txt");
    let steps = input
        .lines()
        .map(RebootStep::from_str)
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    let bounds = Cuboid::from_coords(((-50, 50), (-50, 50), (-50, 50))).unwrap();
    let mut reactor = Reactor::new_with_bounds(bounds);

    for step in steps {
        reactor.execute(step);
    }
    assert_eq!(reactor.cubes_on_count(), 590784);
}

#[test]
fn test_large_example() {
    let input = include_str!("large_example.txt");
    let steps = input
        .lines()
        .map(RebootStep::from_str)
        .collect::<Result<Vec<_>, _>>()
        .unwrap();

    let mut reactor = Reactor::new();

    for step in steps {
        reactor.execute(step);
    }
    assert_eq!(reactor.cubes_on_count(), 2758514936282235);
}
