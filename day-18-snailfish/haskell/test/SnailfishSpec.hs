module SnailfishSpec (spec) where

import Data.Foldable (for_)
import Test.Hspec
import Snailfish (Number, Element(..), add, reduce, reduceStar, magnitude)

spec :: Spec
spec =
  describe "Snailfish" $ do
    describe "reduce" $
      let
        test (input, expected) =
          it (show input) $ reduce input `shouldBe` Just expected
        cases =
          [ ( (P (P (P (P (R 9, R 8), R 1), R 2), R 3), R 4)
            , (P (P (P (R 0, R 9), R 2), R 3), R 4))
          , ( (R 7, P (R 6, P (R 5, P (R 4, P (R 3, R 2)))))
            , (R 7, P (R 6, P (R 5, P (R 7, R 0)))))
          , ( (P (R 6, P (R 5, P (R 4, P (R 3, R 2)))), R 1)
            , (P (R 6, P (R 5, P (R 7, R 0))), R 3))
          , ( (P (R 3, P (R 2, P (R 1, P (R 7, R 3)))), P (R 6, P (R 5, P (R 4, P (R 3, R 2)))))
            , (P (R 3, P (R 2, P (R 8, R 0))), P (R 9, P (R 5, P (R 4, P (R 3, R 2))))))
          , ( (P (R 3, P (R 2, P (R 8, R 0))), P (R 9, P (R 5, P (R 4, P (R 3, R 2)))))
            , (P (R 3, P (R 2, P (R 8, R 0))), P (R 9, P (R 5, P (R 7, R 0)))))
          , ( (P (P (P (R 0, R 7), R 4), P (R 15, P (R 0, R 13))), P (R 1, R 1))
            , (P (P (P (R 0, R 7), R 4), P (P (R 7, R 8), P (R 0, R 13))), P (R 1, R 1)))
          , ( (P (P (P (R 0, R 7), R 4), P (P (R 7, R 8), P (R 0, R 13))), P (R 1, R 1))
            , (P (P (P (R 0, R 7), R 4), P (P (R 7, R 8), P (R 0, P (R 6, R 7)))), P (R 1, R 1)))
          , ( (P (P (P (P (R 4, R 3), R 4), R 4), P (R 7, P (P (R 8, R 4), R 9))), P (R 1, R 1))
            , (P (P (P (R 0, R 7), R 4), P (R 7, P (P (R 8, R 4), R 9))), P (R 1, R 1)))
          , ( (P (P (P (R 0, R 7), R 4), P (R 7, P (P (R 8, R 4), R 9))), P (R 1, R 1))
            , (P (P (P (R 0, R 7), R 4), P (R 15, P (R 0, R 13))), P (R 1, R 1)))
          ]
      in for_ cases test
    describe "reduceStar" $
      let
        test (input, expected) =
          it (show input) $ reduceStar input `shouldBe` expected
        cases =
          [ ( (P (P (P (P (R 4, R 3), R 4), R 4), P (R 7, P (P (R 8, R 4), R 9))), P (R 1, R 1))
            , (P (P (P (R 0, R 7), R 4), P (P (R 7, R 8), P (R 6, R 0))), P (R 8, R 1)))
          ]
      in for_ cases test
    describe "add" $
      let
        test (input, expected) =
          it (show (head input)) $ foldl1 add input `shouldBe` expected
        cases =
          [ ( [ (R 1, R 1)
              , (R 2, R 2)
              , (R 3, R 3)
              , (R 4, R 4)
              ]
            , (P (P (P (R 1, R 1), P (R 2, R 2)), P (R 3, R 3)),P (R 4, R 4))
            )
          , ( [ (R 1, R 1)
              , (R 2, R 2)
              , (R 3, R 3)
              , (R 4, R 4)
              , (R 5, R 5)
              ]
            , (P (P (P (R 3, R 0), P (R 5, R 3)), P (R 4, R 4)),P (R 5, R 5))
            )
          , ( [ (R 1, R 1)
              , (R 2, R 2)
              , (R 3, R 3)
              , (R 4, R 4)
              , (R 5, R 5)
              , (R 6, R 6)
              ]
            , (P (P (P (R 5, R 0), P (R 7, R 4)), P (R 5, R 5)),P (R 6, R 6))
            )
          , ( [ (P (P (R 0, P (R 4, R 5)),P (R 0, R 0)), P (P (P (R 4, R 5), P (R 2,R 6)),P (R 9,R 5)))
              , (R 7,P (P (P (R 3,R 7),P (R 4,R 3)),P (P (R 6,R 3),P (R 8,R 8))))
              ]
            , (P (P (P (R 4,R 0),P (R 5,R 4)),P (P (R 7,R 7),P (R 6,R 0))),P (P (R 8,P (R 7,R 7)),P (P (R 7,R 9),P (R 5,R 0))))
            )
          , ( [ (P (P (P (R 4,R 0),P (R 5,R 4)),P (P (R 7,R 7),P (R 6,R 0))),P (P (R 8,P (R 7,R 7)),P (P (R 7,R 9),P (R 5,R 0))))
              , (P (R 2,P (P (R 0,R 8),P (R 3,R 4))),P (P (P (R 6,R 7),R 1),P (R 7,P (R 1,R 6))))
              ]
            , (P (P (P (R 6,R 7),P (R 6,R 7)),P (P (R 7,R 7),P (R 0,R 7))),P (P (P (R 8,R 7),P (R 7,R 7)),P (P (R 8,R 8),P (R 8,R 0))))
            )
          , ( [ (P (P (P (R 6,R 7),P (R 6,R 7)),P (P (R 7,R 7),P (R 0,R 7))),P (P (P (R 8,R 7),P (R 7,R 7)),P (P (R 8,R 8),P (R 8,R 0))))
              , (P (P (P (R 2,R 4),R 7),P (R 6,P (R 0,R 5))),P (P (P (R 6,R 8),P (R 2,R 8)),P (P (R 2,R 1),P (R 4,R 5))))
              ]
            , (P (P (P (R 7,R 0),P (R 7,R 7)),P (P (R 7,R 7),P (R 7,R 8))),P (P (P (R 7,R 7),P (R 8,R 8)),P (P (R 7,R 7),P (R 8,R 7))))
            )
          , ( [ (P (P (P (R 7,R 0),P (R 7,R 7)),P (P (R 7,R 7),P (R 7,R 8))),P (P (P (R 7,R 7),P (R 8,R 8)),P (P (R 7,R 7),P (R 8,R 7))))
              , (R 7,P (R 5,P (P (R 3,R 8),P (R 1,R 4))))
              ]
            , (P (P (P (R 7,R 7),P (R 7,R 8)),P (P (R 9,R 5),P (R 8,R 7))),P (P (P (R 6,R 8),P (R 0,R 8)),P (P (R 9,R 9),P (R 9,R 0))))
            )
          , ( [ (P (P (P (R 7,R 7),P (R 7,R 8)),P (P (R 9,R 5),P (R 8,R 7))),P (P (P (R 6,R 8),P (R 0,R 8)),P (P (R 9,R 9),P (R 9,R 0))))
              , (P (R 2,P (R 2,R 2)),P (R 8,P (R 8,R 1)))
              ]
            , (P (P (P (R 6,R 6),P (R 6,R 6)),P (P (R 6,R 0),P (R 6,R 7))),P (P (P (R 7,R 7),P (R 8,R 9)),P (R 8,P (R 8,R 1))))
            )
          , ( [ (P (P (P (R 6,R 6),P (R 6,R 6)),P (P (R 6,R 0),P (R 6,R 7))),P (P (P (R 7,R 7),P (R 8,R 9)),P (R 8,P (R 8,R 1))))
              , (R 2, R 9)
              ]
            , (P (P (P (R 6,R 6),P (R 7,R 7)),P (P (R 0,R 7),P (R 7,R 7))),P (P (P (R 5,R 5),P (R 5,R 6)),R 9))
            )
          , ( [ (P (P (P (R 6,R 6),P (R 7,R 7)),P (P (R 0,R 7),P (R 7,R 7))),P (P (P (R 5,R 5),P (R 5,R 6)),R 9))
              , (R 1,P (P (P (R 9,R 3),R 9),P (P (R 9,R 0),P (R 0,R 7))))
              ]
            , (P (P (P (R 7,R 8),P (R 6,R 7)),P (P (R 6,R 8),P (R 0,R 8))),P (P (P (R 7,R 7),P (R 5,R 0)),P (P (R 5,R 5),P (R 5,R 6))))
            )
          , ( [ (P (P (P (R 7,R 8),P (R 6,R 7)),P (P (R 6,R 8),P (R 0,R 8))),P (P (P (R 7,R 7),P (R 5,R 0)),P (P (R 5,R 5),P (R 5,R 6))))
              , (P (P (R 5,P (R 7,R 4)),R 7),R 1)
              ]
            , (P (P (P (R 7,R 7),P (R 7,R 7)),P (P (R 8,R 7),P (R 8,R 7))),P (P (P (R 7,R 0),P (R 7,R 7)),R 9))
            )
          , ( [ (P (P (P (R 7,R 7),P (R 7,R 7)),P (P (R 8,R 7),P (R 8,R 7))),P (P (P (R 7,R 0),P (R 7,R 7)),R 9))
              , (P (P (P (R 4,R 2),R 2),R 6),P (R 8,R 7))
              ]
            , (P (P (P (R 8,R 7),P (R 7,R 7)),P (P (R 8,R 6),P (R 7,R 7))),P (P (P (R 0,R 7),P (R 6,R 6)),P (R 8,R 7)))
            )
          , ( [ (P (P (R 0, P (R 4, R 5)), P (R 0, R 0)), P (P (P (R 4, R 5), P (R 2, R 6)), P (R 9, R 5)))
              , (R 7, P (P (P (R 3, R 7), P (R 4, R 3)), P (P (R 6, R 3), P (R 8, R 8))))
              , (P (R 2, P (P (R 0, R 8), P (R 3, R 4))), P (P (P (R 6, R 7), R 1), P (R 7, P (R 1, R 6))))
              , (P (P (P (R 2, R 4), R 7), P (R 6, P (R 0, R 5))), P (P (P (R 6, R 8), P (R 2, R 8)), P (P (R 2, R 1), P (R 4, R 5))))
              , (R 7, P (R 5, P (P (R 3, R 8), P (R 1, R 4))))
              , (P (R 2, P (R 2, R 2)), P (R 8, P (R 8, R 1)))
              , (R 2, R 9)
              , (R 1, P (P (P (R 9, R 3), R 9), P (P (R 9, R 0), P (R 0, R 7))))
              , (P (P (R 5, P (R 7, R 4)), R 7), R 1)
              , (P (P (P (R 4, R 2), R 2), R 6), P (R 8, R 7))
              ]
            , (P (P (P (R 8, R 7), P (R 7, R 7)), P (P (R 8, R 6), P (R 7, R 7))), P (P (P (R 0, R 7), P (R 6, R 6)), P (R 8, R 7)))
            )
          ]
      in for_ cases test
    describe "magnitude" $ it "should work" $ magnitude (P (R 1, R 2), P (P (R 3, R 4), R 5)) `shouldBe` 143
