module Snailfish.ParserSpec (spec) where

import Data.Foldable (for_)
import Test.Hspec
import Snailfish (Number, Element(..))
import Snailfish.Parser (numbers)

spec :: Spec
spec =
  describe "Snailfish.Parser" $
    describe "numbers" $
      let
        test (input, expected) =
          it (show input) $ numbers "(unknown)" input `shouldBe` Right expected
        cases =
          [ ( "[1,2]"
            , [(R 1, R 2)])
          , ( "[[1,2],3]"
            , [(P (R 1, R 2), R 3)])
          , ( "[[[0,[4,5]],[0,0]],[[[4,5],[2,6]],[9,5]]]\n\
              \[7,[[[3,7],[4,3]],[[6,3],[8,8]]]]\n\
              \[[2,[[0,8],[3,4]]],[[[6,7],1],[7,[1,6]]]]\n\
              \[[[[2,4],7],[6,[0,5]]],[[[6,8],[2,8]],[[2,1],[4,5]]]]\n\
              \[7,[5,[[3,8],[1,4]]]]\n\
              \[[2,[2,2]],[8,[8,1]]]\n\
              \[2,9]\n\
              \[1,[[[9,3],9],[[9,0],[0,7]]]]\n\
              \[[[5,[7,4]],7],1]\n\
              \[[[[4,2],2],6],[8,7]]"
            , [ (P (P (R 0, P (R 4, R 5)),P (R 0, R 0)),P (P (P (R 4, R 5),P (R 2, R 6)),P (R 9, R 5)))
              , (R 7, P (P (P (R 3, R 7),P (R 4, R 3)),P (P (R 6, R 3),P (R 8, R 8))))
              , (P (R 2, P (P (R 0, R 8),P (R 3, R 4))),P (P (P (R 6, R 7),R 1),P (R 7, P (R 1, R 6))))
              , (P (P (P (R 2, R 4),R 7),P (R 6, P (R 0, R 5))),P (P (P (R 6, R 8),P (R 2, R 8)),P (P (R 2, R 1),P (R 4, R 5))))
              , (R 7, P (R 5, P (P (R 3, R 8),P (R 1, R 4))))
              , (P (R 2, P (R 2, R 2)),P (R 8, P (R 8, R 1)))
              , (R 2, R 9)
              , (R 1, P (P (P (R 9, R 3),R 9),P (P (R 9, R 0),P (R 0, R 7))))
              , (P (P (R 5, P (R 7, R 4)),R 7),R 1)
              , (P (P (P (R 4, R 2),R 2),R 6),P (R 8, R 7))
              ])
          ]
      in for_ cases test
