module Main where

import Options.Applicative
import Control.Monad (join)
import Data.List (tails)
import Data.Semigroup ((<>))
import System.IO
import Snailfish
import Snailfish.Parser

argMatch :: Parser (IO ())
argMatch =
  subparser
    ( command "add" (info (addNumbers <$> argument str (metavar "FILE")) idm)
      <> command "largest-magnitude" (info (largestMagnitude <$> argument str (metavar "FILE")) idm)
      <> command "parse" (info (pure parseNumbers) idm)
    )

main :: IO ()
main = join $ execParser opts
  where
    opts = info (argMatch <**> helper) (progDesc "Snailfish")

addNumbers :: String -> IO ()
addNumbers filename = do
  handle <- openFile filename ReadMode
  contents <- hGetContents handle
  case numbers filename contents of
    Left err -> print err
    Right ns -> print $ magnitude $ foldl1 add ns
  hClose handle

parseNumbers :: IO ()
parseNumbers = do
  contents <- getContents
  case numbers "(stdin)" contents of
    Left err -> print err
    Right ns -> print ns

largestMagnitude :: String -> IO ()
largestMagnitude filename = do
  handle <- openFile filename ReadMode
  contents <- hGetContents handle
  case numbers filename contents of
    Left err -> print err
    Right ns -> print . maximum . map magnitude . sums $ ns
  hClose handle
  where
    sums ns = [ add m n | (m:ms) <- tails ns , n <- ms ] ++ [ add n m | (m:ms) <- tails ns , n <- ms ]
