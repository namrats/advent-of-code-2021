{-# LANGUAGE TupleSections #-}

module Snailfish
  ( Element(..)
  , Number
  , add
  , reduce
  , reduceStar
  , magnitude
  ) where

import Control.Applicative ((<|>))

data Element
  = R Integer
  | P (Element, Element)
  deriving (Eq, Show)

type Number = (Element, Element)

add :: Number -> Number -> Number
add x y = reduceStar (P x, P y)

reduce :: Number -> Maybe Number
reduce n = explode n <|> split n

explode :: Number -> Maybe Number
explode n = (\(_, n, _) -> n) <$> explodeDepth 0 n
  where
    explodeDepth depth (a, b) =
      do
        (x, a_, y) <- explodeElement depth a
        return (x, (a_, integrateLeft y b), Nothing)
      <|>
      do
        (x, b_, y) <- explodeElement depth b
        return (Nothing, (integrateRight x a, b_), y)
    explodeElement _ (R x) = Nothing
    explodeElement depth (P (R x, R y)) | depth >= 3 = return (Just x, R 0, Just y)
    explodeElement depth (P a) =
      do
        (x, a_, y) <- explodeDepth (depth + 1) a
        return (x, P a_, y)

    integrateLeft Nothing a = a
    integrateLeft (Just x) (R y) = R (x + y)
    integrateLeft x (P (a, b)) = P (integrateLeft x a, b)

    integrateRight Nothing a = a
    integrateRight (Just x) (R y) = R (x + y)
    integrateRight x (P (a, b)) = P (a, integrateRight x b)

split :: Number -> Maybe Number
split (a, b) =
  (, b) <$> splitElement a <|> (a,) <$> splitElement b
  where
    splitElement (P (a, b)) = P <$> split (a, b)
    splitElement (R x)
      | x >= 10 = let (half_x, r) = x `quotRem` 2 in Just $ P (R half_x, R (half_x + r))
      | otherwise = Nothing

reduceStar :: Number -> Number
reduceStar n =
  maybe n reduceStar (reduce n)

magnitude :: Number -> Integer
magnitude (a, b) = 3 * elementMagnitude a + 2 * elementMagnitude b
  where
    elementMagnitude (R x) = x
    elementMagnitude (P n) = magnitude n
