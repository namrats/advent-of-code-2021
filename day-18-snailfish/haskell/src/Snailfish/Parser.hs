module Snailfish.Parser (number, numbers) where

import Text.ParserCombinators.Parsec
import Snailfish (Number, Element(..))

numbers :: String -> String -> Either ParseError [Number]
numbers = parse ((number `sepEndBy` newline) <* eof)

number :: Parser Number
number = between (char '[') (char ']') elementPair
  where
    elementPair = do
      a <- element
      char ','
      b <- element
      return (a, b)
    element =
      do
        n <- number
        return $ P n
      <|> do
        ds <- many digit
        return $ R $ read ds
