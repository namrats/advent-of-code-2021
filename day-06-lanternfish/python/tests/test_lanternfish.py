import pytest
from lanternfish import run_simulation


@pytest.fixture()
def initial_timers():
    return [3, 4, 3, 1, 2]


@pytest.mark.parametrize(
    "day_count,expected_timers",
    [
        (1, [2, 3, 2, 0, 1]),
        (2, [1, 2, 1, 6, 0, 8]),
        (3, [0, 1, 0, 5, 6, 7, 8]),
        (4, [6, 0, 6, 4, 5, 6, 7, 8, 8]),
        (5, [5, 6, 5, 3, 4, 5, 6, 7, 7, 8]),
        (6, [4, 5, 4, 2, 3, 4, 5, 6, 6, 7]),
        (7, [3, 4, 3, 1, 2, 3, 4, 5, 5, 6]),
        (8, [2, 3, 2, 0, 1, 2, 3, 4, 4, 5]),
        (9, [1, 2, 1, 6, 0, 1, 2, 3, 3, 4, 8]),
        (10, [0, 1, 0, 5, 6, 0, 1, 2, 2, 3, 7, 8]),
        (11, [6, 0, 6, 4, 5, 6, 0, 1, 1, 2, 6, 7, 8, 8, 8]),
        (12, [5, 6, 5, 3, 4, 5, 6, 0, 0, 1, 5, 6, 7, 7, 7, 8, 8]),
        (13, [4, 5, 4, 2, 3, 4, 5, 6, 6, 0, 4, 5, 6, 6, 6, 7, 7, 8, 8]),
        (14, [3, 4, 3, 1, 2, 3, 4, 5, 5, 6, 3, 4, 5, 5, 5, 6, 6, 7, 7, 8]),
        (15, [2, 3, 2, 0, 1, 2, 3, 4, 4, 5, 2, 3, 4, 4, 4, 5, 5, 6, 6, 7]),
        (16, [1, 2, 1, 6, 0, 1, 2, 3, 3, 4, 1, 2, 3, 3, 3, 4, 4, 5, 5, 6, 8]),
        (17, [0, 1, 0, 5, 6, 0, 1, 2, 2, 3, 0, 1, 2, 2, 2, 3, 3, 4, 4, 5, 7, 8]),
        (18, [6, 0, 6, 4, 5, 6, 0, 1, 1, 2, 6, 0, 1, 1, 1, 2, 2, 3, 3, 4, 6, 7, 8, 8, 8, 8]),
    ],
)
def test_example(initial_timers, day_count, expected_timers):
    timers = run_simulation(initial_timers, day_count).elements()
    assert sorted(timers) == sorted(expected_timers)


@pytest.mark.parametrize(
    "day_count,expected_total",
    [
        (80, 5934),
        (256, 26984457539),
    ],
)
def test_example_total(initial_timers, day_count, expected_total):
    assert sum(run_simulation(initial_timers, day_count).values()) == expected_total
