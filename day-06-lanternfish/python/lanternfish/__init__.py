from collections import Counter

NEW_TIMER = 8
OLD_TIMER = 6


def run_simulation(initial_timers: list[int], day_count: int) -> Counter[int]:
    timers = Counter(initial_timers)
    for _ in range(day_count):
        new_timers = Counter[int]()
        for timer, count in timers.items():
            if timer == 0:
                new_timers[OLD_TIMER] += count
                new_timers[NEW_TIMER] += count
            else:
                new_timers[timer - 1] += count
        timers = new_timers

    return timers
