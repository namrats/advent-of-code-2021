import click
from . import run_simulation


@click.command()
@click.argument("file", type=click.File("r"))
@click.argument("duration", type=click.INT, default=80)
def main(file, duration):
    initial_timers = map(int, file.readline().split(","))
    print(sum(run_simulation(initial_timers, duration).values()))
