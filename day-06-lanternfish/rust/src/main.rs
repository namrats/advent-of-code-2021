use lanternfish::run_simulation;
use clap::{App, Arg};
use std::fs::File;
use std::io::{BufRead, BufReader};

fn main() {
    let matches = App::new("lanternfish")
        .arg(Arg::with_name("input").required(true))
        .arg(Arg::with_name("duration"))
        .get_matches();
    let input: Vec<_> = BufReader::new(File::open(matches.value_of("input").unwrap()).unwrap())
        .lines()
        .next()
        .unwrap()
        .unwrap()
        .split(",")
        .map(|n| n.parse::<usize>().unwrap())
        .collect();
    let day_count = matches.value_of("duration").map(|n| n.parse::<usize>().unwrap()).unwrap_or(80);
    let timers = run_simulation(&input, day_count);
    println!("{}", timers.into_values().sum::<usize>());
}
