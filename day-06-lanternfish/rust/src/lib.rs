use std::collections::HashMap;
use itertools::Itertools;

const NEW_TIMER: usize = 8;
const OLD_TIMER: usize = 6;

pub fn run_simulation(initial_timers: &[usize], day_count: usize) -> HashMap<usize, usize> {
    let mut timers = initial_timers.iter().cloned().counts();

    for _ in 0..day_count {
        let mut new_timers = HashMap::new();
        for (timer, count) in timers {
            if timer == 0 {
                *new_timers.entry(OLD_TIMER).or_default() += count;
                *new_timers.entry(NEW_TIMER).or_default() += count;
            } else {
                *new_timers.entry(timer - 1).or_default() += count;
            }
        }
        timers = new_timers;
    }

    timers
}

#[cfg(test)]
mod tests {
    use super::*;
    use rstest::{fixture, rstest};

    #[fixture]
    fn initial_timers() -> Vec<usize> {
        vec![3, 4, 3, 1, 2]
    }

    #[rstest]
    #[case(1, &[2,3,2,0,1])]
    #[case(2, &[1,2,1,6,0,8])]
    #[case(3, &[0,1,0,5,6,7,8])]
    #[case(4, &[6,0,6,4,5,6,7,8,8])]
    #[case(5, &[5,6,5,3,4,5,6,7,7,8])]
    #[case(6, &[4,5,4,2,3,4,5,6,6,7])]
    #[case(7, &[3,4,3,1,2,3,4,5,5,6])]
    #[case(8, &[2,3,2,0,1,2,3,4,4,5])]
    #[case(9, &[1,2,1,6,0,1,2,3,3,4,8])]
    #[case(10, &[0,1,0,5,6,0,1,2,2,3,7,8])]
    #[case(11, &[6,0,6,4,5,6,0,1,1,2,6,7,8,8,8])]
    #[case(12, &[5,6,5,3,4,5,6,0,0,1,5,6,7,7,7,8,8])]
    #[case(13, &[4,5,4,2,3,4,5,6,6,0,4,5,6,6,6,7,7,8,8])]
    #[case(14, &[3,4,3,1,2,3,4,5,5,6,3,4,5,5,5,6,6,7,7,8])]
    #[case(15, &[2,3,2,0,1,2,3,4,4,5,2,3,4,4,4,5,5,6,6,7])]
    #[case(16, &[1,2,1,6,0,1,2,3,3,4,1,2,3,3,3,4,4,5,5,6,8])]
    #[case(17, &[0,1,0,5,6,0,1,2,2,3,0,1,2,2,2,3,3,4,4,5,7,8])]
    #[case(18, &[6,0,6,4,5,6,0,1,1,2,6,0,1,1,1,2,2,3,3,4,6,7,8,8,8,8])]
    fn test_example(initial_timers: Vec<usize>, #[case] day_count: usize, #[case] expected_timers: &[usize]) {
        let timers = run_simulation(&initial_timers, day_count);
        assert_eq!(timers, expected_timers.iter().cloned().counts());
    }

    #[rstest]
    #[case(80, 5934)]
    #[case(256, 26984457539)]
    fn test_example_total(initial_timers: Vec<usize>, #[case] day_count: usize, #[case] expected_total: usize) {
        let total: usize = run_simulation(&initial_timers, day_count).into_values().sum();
        assert_eq!(total, expected_total);
    }
}
