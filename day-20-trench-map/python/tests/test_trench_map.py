import pytest
from inspect import cleandoc
from trench_map import Enhancer, Image


class TestExample:
    @pytest.fixture
    def enhancer(self):
        return Enhancer(
            "..#.#..#####.#.#.#.###.##.....###.##.#..###.####..#####..#....#..#..##..##\
#..######.###...####..#..#####..##..#.#####...##.#.#..#.##..#.#......#.###\
.######.###.####...#.##.##..#..#..#####.....#.#....###..#.##......#.....#.\
.#..#..##..#...##.######.####.####.#.#...#.......#..#.#.#...####.##.#.....\
.#..#...##.#.##..#...##.#.##..###.#......#.#.......#.#.#.####.###.##...#..\
...####.#..#..#.##.#....##..#.####....##...##..#...#......#.#.......#.....\
..##..####..#...#.#.#...##..#.#..###..#####........#..####......#..#"
        )

    @pytest.fixture
    def image(self):
        return Image.load(
            cleandoc(
                """
            #..#.
            #....
            ##..#
            ..#..
            ..###
        """
            )
        )

    @pytest.mark.parametrize(
        "times,expected",
        [
            (2, 35),
            (50, 3351),
        ],
    )
    def test_example(self, enhancer, image, times, expected):
        enhanced_image = enhancer.enhance(image, times=times)
        assert sum(enhanced_image.pixels()) == expected
