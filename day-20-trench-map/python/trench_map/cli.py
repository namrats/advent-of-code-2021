import click
from trench_map import Image, Enhancer


@click.command()
@click.argument("file", type=click.File("r"))
@click.option("-n", "times", default=2)
def main(file, times):
    (algorithm, image_data) = file.read().split("\n\n")

    enhancer = Enhancer(algorithm)
    image = Image.load(image_data)

    enhanced_image = enhancer.enhance(image, times=times)

    print(sum(enhanced_image.pixels()))
