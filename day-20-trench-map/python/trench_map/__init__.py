from itertools import chain, count
from more_itertools import windowed
from typing import Iterator


class Image:
    def __init__(self, data: list[list[bool]], edge: bool = False):
        self.data = data
        self.edge = edge
        self.height = len(data)
        self.width = len(data[0])

    @classmethod
    def load(cls, data: str) -> "Image":
        return cls([[pixel == "#" for pixel in row] for row in data.splitlines()])

    def pixels(self) -> Iterator[bool]:
        return chain(*self.data)

    def windows(self) -> Iterator:
        return (
            [
                tuple(chain(*w))
                for w in zip(
                    *(
                        windowed(chain([self.edge] * 2, row, [self.edge] * 2), 3)
                        for row in rows
                    )
                )
            ]
            for rows in windowed(
                chain(
                    [[self.edge] * self.width] * 2,
                    self.data,
                    [[self.edge] * self.width] * 2,
                ),
                3,
            )
        )

    def __str__(self) -> str:
        return "\n".join(
            "".join("#" if pixel else "." for pixel in row) for row in self.data
        )


class Enhancer:
    def __init__(self, algorithm):
        self.algorithm = [cell == "#" for cell in algorithm]

    @staticmethod
    def pixels_to_int(pixels) -> int:
        return sum(p for d, p in zip(reversed(pixels), (2 ** i for i in count())) if d)

    def enhance(self, image: Image, /, times: int = 1) -> Image:
        for _ in range(times):
            image = Image(
                [
                    [self.algorithm[self.pixels_to_int(pixels)] for pixels in row]
                    for row in image.windows()
                ],
                edge=self.algorithm[self.pixels_to_int([image.edge] * 9)],
            )
        return image
