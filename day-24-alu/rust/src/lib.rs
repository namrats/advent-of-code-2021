pub mod parser;

use std::collections::HashMap;

type Value = i64;

#[derive(Debug)]
pub enum Expression {
    Var(char),
    Num(Value),
}

#[derive(Debug)]
pub enum Instruction {
    Input(char),
    Add(char, Expression),
    Mul(char, Expression),
    Div(char, Expression),
    Mod(char, Expression),
    Equal(char, Expression),
}

use Expression::*;
use Instruction::*;

#[derive(Debug)]
pub struct Program {
    instructions: Vec<Instruction>,
}

impl Program {
    pub fn new(instructions: Vec<Instruction>) -> Self {
        Self { instructions }
    }

    pub fn solve(&self) -> Vec<i64> {
        let constants = self.instructions.chunks(18)
            .map(|chunk| {
                let a = match chunk[4] {
                    Div(_, Num(n)) => n,
                    _ => unreachable!(),
                };
                let b = match chunk[5] {
                    Add(_, Num(n)) => n,
                    _ => unreachable!(),
                };
                let c = match chunk[15] {
                    Add(_, Num(n)) => n,
                    _ => unreachable!(),
                };
                (a, b, c)
            })
            .collect::<Vec<_>>();

        let mut stack = Vec::new();
        let mut result = vec![0 ; 14];
        for (i, (a, b, c)) in constants.into_iter().enumerate() {
            if a == 1 {
                stack.push((i, c));
            } else {
                let (j, c) = stack.pop().unwrap();
                let d = b + c;
                result[j] = (1..=9).filter(|n| (1..=9).contains(&(n + d))).min().unwrap();
                result[i] = (1..=9).filter(|n| (1..=9).contains(&(n - d))).min().unwrap();
            }
        }

        result
    }
}

#[derive(Debug)]
pub struct Alu {
    registers: HashMap<char, Value>,
}

impl Alu {
    pub fn new() -> Self {
        let registers = ('w'..='z').map(|r| (r, 0)).collect();
        Alu { registers }
    }

    pub fn execute<I>(&mut self, program: &Program, input: I)
    where I: IntoIterator<Item=Value>
    {
        let mut input = input.into_iter();
        for instruction in &program.instructions {
            self.execute_instruction(instruction, &mut input);
            // println!("{:?}", instruction);
            // println!("w={} x={} y={} z={}", self.get(&'w'), self.get(&'x'), self.get(&'y'), self.get(&'z'));
        }
    }

    fn eval_expr(&self, expr: &Expression) -> Value {
        match expr {
            Var(register) => self.registers[register],
            Num(value) => *value,
        }
    }

    fn set(&mut self, register: &char, value: Value) {
        self.registers.insert(*register, value);
    }

    pub fn get(&self, register: &char) -> Value {
        self.registers[register]
    }

    fn execute_instruction<I>(&mut self, instruction: &Instruction, input: &mut I)
    where I: Iterator<Item=Value>
    {
        match instruction {
            Input(register) => self.set(register, input.next().unwrap()),
            Add(register, expr) => {
                let value = self.get(register) + self.eval_expr(expr);
                self.set(register, value);
            }
            Mul(register, expr) => {
                let value = self.get(register) * self.eval_expr(expr);
                self.set(register, value);
            }
            Div(register, expr) => {
                let value = self.get(register) / self.eval_expr(expr);
                self.set(register, value);
            }
            Mod(register, expr) => {
                let value = self.get(register) % self.eval_expr(expr);
                self.set(register, value);
            }
            Equal(register, expr) => {
                let value = (self.get(register) == self.eval_expr(expr)) as Value;
                self.set(register, value);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::str::FromStr;
    use indoc::indoc;

    #[test]
    fn test_small_program() {
        let program = Program::from_str(
            indoc!{"
            inp x
            mul x -1
            "}
        ).unwrap();

        println!("{:?}", program);

        let mut alu = Alu::new();
        alu.execute(&program, vec![3]);

        assert_eq!(alu.registers[&'x'], -3);
    }

    #[test]
    fn test_medium_program() {
        let program = Program::from_str(
            indoc!{"
            inp z
            inp x
            mul z 3
            eql z x
            "}
        ).unwrap();

        let mut alu = Alu::new();
        alu.execute(&program, vec![1, 3]);

        assert_eq!(alu.registers[&'z'], 1);

        let mut alu = Alu::new();
        alu.execute(&program, vec![2, 3]);

        assert_eq!(alu.registers[&'z'], 0);
    }

    #[test]
    fn test_large_program() {
        let program = Program::from_str(
            indoc!{"
            inp w
            add z w
            mod z 2
            div w 2
            add y w
            mod y 2
            div w 2
            add x w
            mod x 2
            div w 2
            mod w 2
            "}
        ).unwrap();

        let mut alu = Alu::new();
        alu.execute(&program, vec![3]);

        assert_eq!(alu.registers[&'w'], 0);
        assert_eq!(alu.registers[&'x'], 0);
        assert_eq!(alu.registers[&'y'], 1);
        assert_eq!(alu.registers[&'z'], 1);
    }
}
