use std::str::FromStr;
use std::fs::read_to_string;
use alu::{Alu, Program};
use clap::{App, Arg};

fn main() {
    let matches = App::new("alu")
        .arg(Arg::with_name("input").required(true))
        .get_matches();

    let program = Program::from_str(&read_to_string(matches.value_of("input").unwrap()).unwrap()).unwrap();

    let model_number: Vec<i64> = program.solve();

    print!("{}", model_number.iter().map(ToString::to_string).collect::<Vec<_>>().join(""));

    let mut alu = Alu::new();
    alu.execute(&program, model_number);

    if alu.get(&'z') == 0 {
        println!(" ✅");
    } else {
        println!(" ❎");
    }
}
