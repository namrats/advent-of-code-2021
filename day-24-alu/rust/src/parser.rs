use nom::{
    self, branch::alt, bytes::complete::tag, multi::separated_list1, character::complete::{anychar, i64, space1, newline}, combinator::map,
    sequence::tuple, Finish, IResult,
};
use std::str::FromStr;
use super::{Expression, Instruction, Program};
use Expression::*;
use Instruction::*;

#[derive(Debug)]
pub struct ParseError(String);

fn expression(input: &str) -> IResult<&str, Expression> {
    alt((
        map(i64, Num),
        map(anychar, Var),
    ))(input)
}

fn instruction(input: &str) -> IResult<&str, Instruction> {
    alt((
        map(tuple((tag("inp"), space1, anychar)),
            |x| Input(x.2)),
        map(tuple((tag("add"), space1, anychar, space1, expression)),
            |x| Add(x.2, x.4)),
        map(tuple((tag("mul"), space1, anychar, space1, expression)),
            |x| Mul(x.2, x.4)),
        map(tuple((tag("div"), space1, anychar, space1, expression)),
            |x| Div(x.2, x.4)),
        map(tuple((tag("mod"), space1, anychar, space1, expression)),
            |x| Mod(x.2, x.4)),
        map(tuple((tag("eql"), space1, anychar, space1, expression)),
            |x| Equal(x.2, x.4)),
    ))(input)
}

impl FromStr for Instruction {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        instruction(s)
            .finish()
            .map(|(_, result)| result)
            .map_err(|e| ParseError(format!("{}", e)))
    }
}

fn program(input: &str) -> IResult<&str, Program> {
    map(separated_list1(newline, instruction), Program::new)(input)
}

impl FromStr for Program {
    type Err = ParseError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        program(s)
            .finish()
            .map(|(_, result)| result)
            .map_err(|e| ParseError(format!("{}", e)))
    }
}
