module Alu.Compiler
    ( SymbolicExpression(..)
    , compile
    , simplify
    )
    where

import Alu

data SymbolicExpression
  = SEInp Int
  | SEVal Value
  | SEReg Register
  | SEBinOp Operator SymbolicExpression SymbolicExpression
  deriving Eq

instance Show SymbolicExpression where
  show (SEInp i) = "I" ++ show i
  show (SEReg r) = [r]
  show (SEVal v) = show v
  show (SEBinOp op e1 e2) = "(" ++ unwords [show e1, show op, show e2] ++ ")"

type Compiler = (Register -> SymbolicExpression, Int)

boolValue :: Bool -> Value
boolValue = toInteger . fromEnum

evalOp :: Operator -> Value -> Value -> Value
evalOp OAdd = (+)
evalOp OMul = (*)
evalOp ODiv = div
evalOp OMod = mod
evalOp OEql = (boolValue .) . (==)
evalOp ONeq = (boolValue .) . (/=)

compile :: Program -> Register -> SymbolicExpression
compile (Program instructions) = fst $ foldl compileInstruction (SEReg, 0) instructions
  where
    compileInstruction (sr, n) (IInp r0) =
      (\r -> if r == r0 then SEInp n else sr r, succ n)
    compileInstruction (sr, n) (IBinOp op r0 e) =
      (\r -> if r == r0 then SEBinOp op (sr r0) (compileExpr sr e) else sr r, n)
    compileExpr _ (EVal v) = SEVal v
    compileExpr sr (EReg r) = sr r


simplify :: SymbolicExpression -> SymbolicExpression
simplify (SEBinOp op e1 e2) =
  case (op, simplify e1, simplify e2) of
    (OMul, SEVal 0, _) -> SEVal 0
    (OMul, _, SEVal 0) -> SEVal 0
    (OAdd, SEVal 0, e) -> e
    (OAdd, e, SEVal 0) -> e
    (OMul, SEVal 1, e) -> e
    (OMul, e, SEVal 1) -> e
    (ODiv, e, SEVal 1) -> e
    (OEql, SEInp _, SEVal n) | n > 9 -> SEVal 0
    (OEql, SEVal n, SEInp _) | n > 9 -> SEVal 0
    (OMod, e@(SEBinOp OAdd (SEInp _) (SEVal n1)), SEVal n2) | n1 + 9 < n2 -> e
    (OEql, SEBinOp OAdd (SEInp _) (SEVal n), SEInp _) | n > 9 -> SEVal 0
    (OEql, SEBinOp OEql e1 e2, SEVal 0) -> SEBinOp ONeq e1 e2
    (OAdd, SEBinOp OAdd (SEInp i) (SEVal v1), SEVal v2) -> SEBinOp OAdd (SEInp i) (SEVal (evalOp OAdd v1 v2))
    (op, SEVal v1, SEVal v2) -> SEVal (evalOp op v1 v2)
    (op, e1, e2) -> SEBinOp op e1 e2
simplify e = e
