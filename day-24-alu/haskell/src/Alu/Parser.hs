module Alu.Parser (parseProgram) where

import Text.ParserCombinators.Parsec
import Alu

parseProgram :: String -> Either ParseError Program
parseProgram = parse (program <* eof) "(unknown)"

program :: Parser Program
program = Program <$> instruction `sepEndBy` newline

register :: Parser Register
register = anyChar

value :: Parser Value
value = do
  sign <- option "" (string "-")
  ds <- many1 digit
  return $ read (sign ++ ds)

expression :: Parser Expression
expression = EVal <$> value <|> EReg <$> register

operator :: Parser Operator
operator =
  (string "add" >> return OAdd)
  <|> try (string "mul" >> return OMul)
  <|> (string "div" >> return ODiv)
  <|> (string "mod" >> return OMod)
  <|> (string "eql" >> return OEql)

instruction :: Parser Instruction
instruction =
  do
    string "inp"
    spaces
    r <- register
    return $ IInp r
  <|>
  do
    op <- operator
    spaces
    r <- register
    spaces
    e <- expression
    return $ IBinOp op r e
