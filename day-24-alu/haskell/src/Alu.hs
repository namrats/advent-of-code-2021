module Alu
    ( Program(..)
    , Instruction(..)
    , Expression(..)
    , Operator(..)
    , Register
    , Value
    ) where

import Control.Monad (guard, mzero, mplus)

type Value = Integer
type Register = Char

data Expression
  = EVal Value
  | EReg Register
  deriving (Eq, Show)

data Operator
  = OAdd
  | OMul
  | ODiv
  | OMod
  | OEql
  | ONeq
  deriving Eq

instance Show Operator where
  show OAdd = "+"
  show OMul = "*"
  show ODiv = "/"
  show OMod = "%"
  show OEql = "=="
  show ONeq = "!="

data Instruction
  = IInp Register
  | IBinOp Operator Register Expression
  deriving (Eq, Show)

newtype Program = Program [Instruction]
  deriving (Eq, Show)
