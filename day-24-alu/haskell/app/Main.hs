module Main where

import Alu.Parser (parseProgram)
import Alu.Compiler (compile, simplify)

main :: IO ()
main = do
  contents <- getContents
  case parseProgram contents of
    Left err -> print err
    Right program -> print . simplify . compile program $ 'z'
