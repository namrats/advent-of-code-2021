module AluSpec (spec) where

import Test.Hspec
import Alu
import Alu.Parser
import Alu.Compiler

spec :: Spec
spec = describe "Alu" $ do
  describe "parseProgram" $ do
    it "parses input" $
      parseProgram "inp w" `shouldBe` Right (Program [IInp 'w'])
    it "parses mul positive" $
      parseProgram "mul x 0" `shouldBe` Right (Program [IBinOp OMul 'x' (EVal 0)])
    it "parses mul negative" $
      parseProgram "mul x -1" `shouldBe` Right (Program [IBinOp OMul 'x' (EVal (-1))])
    it "parses mul register" $ parseProgram "mul x y" `shouldBe` Right (Program [IBinOp OMul 'x' (EReg 'y')])
  describe "compile" $
    it "compiles" $
      compile (Program [IInp 'w', IInp 'w']) 'w' `shouldBe` SEInp 1
