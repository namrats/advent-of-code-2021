import click
from . import OctopusGrid


@click.group()
def main():
    pass


@main.command("flash-count")
@click.option("-c", "--step-count", default=100)
@click.argument("file", type=click.File("r"))
def flash_count(file, step_count):
    grid = OctopusGrid(file.read())
    print(grid.flash_count(step_count))


@main.command("all-flashing")
@click.argument("file", type=click.File("r"))
def all_flashing(file):
    grid = OctopusGrid(file.read())
    print(grid.all_flashing_step())
