from itertools import count


class OctopusGrid:
    def __init__(self, grid: str):
        self.grid = [[int(cell) for cell in row] for row in grid.splitlines()]
        self.height = len(self.grid)
        self.width = len(self.grid[0])

    def run_simulation(self) -> int:
        for j in range(self.height):
            for i in range(self.width):
                self.grid[j][i] += 1

        while (flashing := self.flashing_octopi()):
            for i, j in flashing:
                self.grid[j][i] = -1
                for m, n in self.neighbors(i, j):
                    if 0 <= self.grid[n][m] <= 9:
                        self.grid[n][m] += 1

        for j in range(self.height):
            for i in range(self.width):
                if self.grid[j][i] < 0:
                    self.grid[j][i] = 0

    def flash_count(self, step_count: int) -> int:
        result = 0
        for step in range(step_count):
            self.run_simulation()
            result += sum(
                1
                for row in self.grid
                for octopus in row
                if octopus == 0)
        return result

    def all_flashing_step(self) -> int:
        for step in count():
            if all(all(octopus == 0 for octopus in row) for row in self.grid):
                return step
            self.run_simulation()

    def flashing_octopi(self):
        return [
            (i, j)
            for j, row in enumerate(self.grid)
            for i, octopus in enumerate(row)
            if octopus > 9
        ]

    def neighbors(self, i, j):
        return [
            (m, n)
            for m in [i-1, i, i+1]
            for n in [j-1, j, j+1]
            if (m, n) != (i, j)
            and m in range(self.width)
            and n in range(self.height)
        ]

    def __eq__(self, other) -> bool:
        return (
            isinstance(other, self.__class__)
            and self.grid == other.grid
        )

    def __str__(self) -> str:
        return self.grid

    def __repr__(self) -> str:
        return f"OctopusGrid(grid={repr(self.grid)})"
